<?php
/*

Code based on Cerebro CRM, also by me
## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   Copyright 2014 Left Join Ltd.
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/

?><!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>University of Edinburgh | Business Travel Reporting</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
	<link rel="stylesheet" href="css/cerebro.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/skeleton-tabs.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">
	
	<!-- JavaScript
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://unpkg.com/vue/dist/vue.min.js"></script>
	<script src="js/skeleton-tabs.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<?php
		 
/**
 * Connects to backend DB
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
class DBconnect {
	public $connection;
	public function connect() {
		set_time_limit(20000);
		$hostname_contacts = "localhost";
		$database_contacts = "srs_businesstravel"; //The name of the database
		$username_contacts = "chiara"; //The username for the database
		$password_contacts = "gTsv64!5"; // The password for the database
		$this->connection = mysqli_connect($hostname_contacts, $username_contacts, $password_contacts, $database_contacts);
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}

// Set up the database
global $connectionmanager;

// Connect to the database
$connectionmanager = new DBConnect();
$connectionmanager->connect();


// Status codes
$status_codes = array(
	'0' => 'New data - still processing',
	
	'100' => 'Calculated using the best possible data',
	'101' => 'Calculated with after converting miles to km',
	'102' => 'Calculated using a fuzzy name match',
	'103' => 'Calculated using a Google Search',
	'104' => 'Calculated after correcting a misspelling',
	
	'200' => 'Calculated using averages, not specific conversion factors',
	
	'300' => 'Calculated using Plugging the Gaps method',
	
	'400' => 'Error: Could not calculate for an unknown reason',
	'401' => 'Error: Could not calculate because an origin or desination is missing',
	'402' => 'Error: Could not calculate because an origin or destination is unresolvable - it might be mispelt?',
	'403' => 'Error: Could not calculate because a journey mode of transport is missing',
	'500' => 'Error: Could not calculate because spend and distance are missing',
	
	'600' => 'Suspiciously long rail journey',
	'601' => 'Suspiciously short flight',
	'602' => 'Possible duplicate',
	'603' => 'Suspiciously expensive air journeys',
	'604' => 'Suspiciously expensive train journeys',
	'700' => 'Possible luggage or visa claims',

);

// Explaining location codes to user

$location_codes = array(
	'code_uk_station'  => 'Code found in UK railway stations list',
	'airport_iata'   => 'International Airport Code',
	'airport_name'   => 'Found in list of airport names',
	'uk_stations'  => 'Found in list of UK railway station names',
	'google_search'   => 'We searched on Google',
	'fuzzy_geoname'  => 'We found a similar name in the OpenStreetMap global database',
	'geoname_uk'   => 'We found a name in the OpenStreetMap database of UK places',
	'geoname'  => 'We found an exact match in the OpenStreetMap database global database',
	'spelling_correction_airport_name'   => 'After a spelling correction, found in list of airport names',
	'london_stations'   => 'Found in the list of London Underground stations',
	'spelling_correction_fuzzy_geoname'   => 'After a spelling correction, we found a similar name in the OpenStreetMap global database',
	'spelling_correction_airport_iata'   => 'After a spelling correction, International Airport Code'
	
);


$google_maps_api_key = 'AIzaSyCNAYBhlI13Jr0twtC1wZs94nnH8K2ccmE';

$codes_for_query = "mode_of_transport = 'air'";
if ($_GET['ccl4'] && $_GET['ccl4'] != 'ALL_OF_THEM' && $_GET['ccl4'] != '') {
	// get CCL4 code
	$search_ccl4 = $_GET['ccl4'];
	
	
	// get the codes associated with this CCL4
	$codes_for_query = '';
		$codes = mysqli_query($connectionmanager->connection, "SELECT * FROM cost_centres WHERE CCL4 = '".mysqli_real_escape_string($connectionmanager->connection,$search_ccl4)."' GROUP BY ccl6");
	$i = 0;
	while ($row = mysqli_fetch_assoc($codes)) {
		if ($i == 0) {
			$codes_for_query .= 'ccl6 = '.$row['CCL6'].' ';
		} else {
			$codes_for_query .= 'OR ccl6 = '.$row['CCL6'].' ';
		}
		$i++;
	}
	
	
	$codes_for_query = '('.$codes_for_query.") AND mode_of_transport = 'air'";
}

if (isset($_GET['hide_depts'])) {
	$departments_array = array();
} else {
	// list of possible departments
	$departments_array = array();
	$departments = mysqli_query($connectionmanager->connection, "SELECT CCN4, CCL4 FROM `cost_centres` WHERE 1 GROUP BY CCL4 ORDER BY CCN4 ASC");
	
	while ($row = mysqli_fetch_assoc($departments)) {
		if ($_GET['ccl4'] == $row['CCL4']) {
			$row['selected'] = 'selected';
		}
		$departments_array[] = $row;
	}
}


//year	

$years_for_query = '';
if ($_GET['year'] && $_GET['year'] != 'ALL_YEARS' && $_GET['year'] != '') {
	//
	if ($codes_for_query == "mode_of_transport = 'air'") {
		$codes_for_query = '';
		$years_for_query = '( year = "'.mysqli_real_escape_string($connectionmanager->connection,$_GET['year']).'")';
	} else {
		$years_for_query = ' AND ( year = "'.mysqli_real_escape_string($connectionmanager->connection,$_GET['year']).'")';
	}
}	
	
	
// get the year array

$years_array = array ();
$years = mysqli_query($connectionmanager->connection, "SELECT year FROM journeys WHERE 1 GROUP BY year DESC");

while ($row = mysqli_fetch_assoc($years)) {
	if ($_GET['year'] == $row['year']) {
		$row['selected'] = 'selected';
	}
	$years_array[] = $row;
}
	
//	


$google_format = array();
mysqli_query($connectionmanager->connection,"SET CHARACTER SET utf8;");
$pins =  mysqli_query($connectionmanager->connection,"SELECT
											journey_id, destination, destination_latitude, destination_longitude, origin, origin_latitude, origin_longitude
											FROM journeys
											WHERE ".$codes_for_query.$years_for_query.""
										);

while ($row =  mysqli_fetch_assoc($pins)) {
	//[["Digital Ambassadors","55.945387","-3.181224",91419]]
	if ($row['destination_latitude'] != null && strpos(strtolower($row['destination']),'edinburgh') === false) {
		$google_format[] = array($row['destination'],$row['destination_latitude'],$row['destination_longitude'],$row['journey_id']);
	}
	if ($row['origin_latitude'] != null && strpos(strtolower($row['origin']),'edinburgh') === false) {
		$google_format[] = array($row['origin'],$row['origin_latitude'],$row['origin_longitude'],$row['journey_id']);
	}
}
		 
	// Display map
  	
	$view = file_get_contents('views/heatmap.html');
  $google_format_encoded = json_encode($google_format);
  //print_r(json_last_error_msg());
	$view = str_replace(array('<<data>>','<<googleapikey>>','<<depts>>'),array($google_format_encoded,$google_maps_api_key,json_encode($departments_array)),$view);
	
  
	echo $view;
										
?>
			
		 

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
