<?php
/*

## University of Edinburgh Business Travel Database
   Copyright 2017 The University of Edinburgh
   
   @author Chiara Aquino
   @author Joseph Farthing
*/

?><!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
  <meta charset="utf-8">
  <title>University of Edinburgh | Business Travel Reporting</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
  <div class="container">
    <div class="row">

<?php


error_reporting(1);


/**
 * Case in-sensitive array_search() with partial matches
 *
 * @param string $needle   The string to search for.
 * @param array  $haystack The array to search in.
 *
 * @author Bran van der Meer <branmovic@gmail.com>
 * @since 29-01-2010
 */
function array_find($needle, array $haystack)
{
    foreach ($haystack as $key => $value) {
        if (false !== stripos($value, $needle)) {
            return $key;
        }
    }
    return false;
}


/**
 * Connects to backend DB
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
class DBconnect {
	public $connection;
	public function connect() {
		set_time_limit(20000);
		$hostname_contacts = "localhost";
		$database_contacts = "srs_businesstravel"; //The name of the database
		$username_contacts = "chiara"; //The username for the database
		$password_contacts = "gTsv64!5"; // The password for the database
		$this->connection = mysqli_connect($hostname_contacts, $username_contacts, $password_contacts, $database_contacts);
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}


$successful = 0;
$failed = 0;
$errors = array();

// Set the limit
$limit = 1500; // the absolute max number of journeys to calculate

// Set up the database
global $connectionmanager;

// Connect to the database
$connectionmanager = new DBConnect();
$connectionmanager->connect();

// Get the DEFRA factors and process into an array
global $cost_centres;
$cost_centres = array();

// Load the defra factor table
$cost_data = mysqli_query($connectionmanager->connection,"SELECT * FROM cost_centres WHERE 1");

// Go through the table and save it to the array
while ($factor = mysqli_fetch_assoc($cost_data)) {
	$cost_centres[] = $factor;
}


// Get the dept_standardisation entries and process them into an array
global $dept_standard;
$dept_standard = array();

// Load the dept_standardisation table
$dept_data = mysqli_query($connectionmanager->connection,"SELECT * FROM dept_standardisation WHERE 1");

// Go through the table and save it to the array
while ($dept_factor = mysqli_fetch_assoc($dept_data)) {
	$dept_standard[strtolower($dept_factor['department'])] = $dept_factor['ccl6'];
}	

//print_r($dept_standard);


echo '<p>Getting the data...</p>';


// Get the data we need to process
$dataset = mysqli_query($connectionmanager->connection,"SELECT * FROM journeys WHERE ccl6 = '' LIMIT ".$limit);
echo '<p>Number of rows: '.mysqli_num_rows($dataset).'</p>';	

echo '
<div class="twelve columns">
<table class="u-max-full-width">';



// For every row of the data we run this
$i = 0;
while ($journey = mysqli_fetch_assoc($dataset)) {
	
	$runtime = round(microtime(TRUE) - $_SERVER['REQUEST_TIME_FLOAT'], 6);


	$found_ccl6 = null;
	
	
	// update journey table 
	
	//$update = mysqli_query($connectionmanager->connection, "UPDATE journeys SET ccl6 = '".$found_ccl6."' WHERE journey_id = ".$journey['journey_id']);
 	
//	}
	
	
	
	
	$rows_to_search = array('other_names', 'CCN9', 'CCL9', 'CCL8', 'CCN8', 'CCL7', 'CCN7', 'CCL6', 'CCN6', 'CCL5', 'CCN5', 'CCL4', 'CCN4', 'CCL3', 'CCN3', 'CCL2', 'CCN2', 'CCL1', 'CCN1');
	if ($journey['cost_centre'] != '' && $journey['cost_centre'] != null) {
	
		$key_saved = null;
		
		foreach ($rows_to_search as $row) {
			$key = array_search($journey['cost_centre'], array_column($cost_centres, $row));
			if ($key > 0) {
				$key_saved = $key;
				break;
			}
		
		}	
		
		$found_ccl6 = $cost_centres[$key_saved];
	
	} else if ($journey['department'] != '' && $journey['department'] != null) {
		
		$key_saved = null;
		$found_ccl6 = array();
		
		if (isset($dept_standard[strtolower($journey['department'])]) && $dept_standard[strtolower($journey['department'])] != null) { // department 
			$found_ccl6['CCL6'] = $dept_standard[strtolower($journey['department'])];
			echo '<p>'.$journey['department'].' = '.$found_ccl6['CCL6'].'</p>';
		} else {
		
		
		
			$remove = array('University of Edinburgh - ',
											'School of',
											'School of',
											'school of',
											'Department of',
											'Department Of',
											'department of',
											'Department For',
											'Department for',
											'department for',
											'& Buildings',
											'and Buildings',
											'And Buildings',
											'Department',
											'department',
											'School',
											'school',
			);
			
			foreach ($rows_to_search as $row) {
				$key = array_find($journey['department'], array_column($cost_centres, $row));
				if ($key > 0) {
					$key_saved = $key;
					break;
				} else {
					$key = array_find(trim(str_replace($remove,'',$journey['department'])), array_column($cost_centres, $row));
					//echo '<p>'.trim(str_replace($remove,'',$journey['department'])).'</p>';
					if ($key > 0) {
						$key_saved = $key;
						break;
					}
				}
			
			}
			
			$found_ccl6 = $cost_centres[$key_saved];
		}
	}
	
	if ($found_ccl6 != null) {
		$try_upload = mysqli_query($connectionmanager->connection, "UPDATE journeys SET ccl6 = '".$found_ccl6['CCL6']."' WHERE journey_id = ".$journey['journey_id']);
		if ( $try_upload === false ){
			$failed++;
			$errors[] = mysqli_error($connectionmanager->connection);
		} else {
			$successful++;
		}
	} else {
		$try_upload = mysqli_query($connectionmanager->connection, "UPDATE journeys SET ccl6 = 'NA' WHERE journey_id = ".$journey['journey_id']);

		
	}
	
	$i++;

}


echo '<p>Successfully edited '.$successful.' journeys out of '.$limit.'.</p>';
echo '</tbody></table></div>';






if (count($errors) > 0) {
		echo '<h2>Database errors</h2><pre>';
		print_r($errors);
		echo '</pre>';
}




?>

    </div>
  </div>

<!-- End Document
    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->
</body>
</html>

