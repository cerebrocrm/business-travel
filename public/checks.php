<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>University of Edinburgh | Business Travel Reporting</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <div class="row">

<?php
/*

Code based on Cerebro CRM, also by me
## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   Copyright 2014 Left Join Ltd.
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
		 
/**
 * Connects to backend DB
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
class DBconnect {
	public $connection;
	public function connect() {
		set_time_limit(20000);
		$hostname_contacts = "localhost";
		$database_contacts = "srs_bustravel"; //The name of the database
		$username_contacts = "srs_bustravel"; //The username for the database
		$password_contacts = "ffMd33*9"; // The password for the database
		$this->connection = mysqli_connect($hostname_contacts, $username_contacts, $password_contacts, $database_contacts);
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}

// Set up the database
global $connectionmanager;

// Connect to the database
$connectionmanager = new DBConnect();
$connectionmanager->connect();


// Status codes
$status_codes = array(
	'0' => 'New data',
	
	'100' => 'Calculated using the best possible data',
	'101' => 'Calculated with after converting miles to km',
	'102' => 'Calculated using a fuzzy name match',
	'103' => 'Calculated using a Google Search',
	'104' => 'Calculated after correcting a misspelling',
	'110' => 'Calculated using Law of Cosines method',
	'120' => 'Calculated using google distance method for rail journeys',
	
	'200' => 'Calculated using averages, not specific conversion factors',
	
	'300' => 'Calculated using Plugging the Gaps method',
	
	'400' => 'Error: Could not calculate for an unknown reason',
	'401' => 'Error: Could not calculate because an origin or desination is missing',
	'402' => 'Error: Could not calculate because an origin or destination is unresolvable - it might be mispelt?',
	'403' => 'Error: Could not calculate because a journey mode of transport is missing',
	'500' => 'Error: Could not calculate because spend and distance are missing',
	
	'600' => 'Suspiciously long rail journey',
	'601' => 'Suspiciously short flight',
	'602' => 'Possible duplicate',
	'603' => 'Suspiciously expensive air journeys',
	'604' => 'Suspiciously expensive train journeys',
	'700' => 'Possible luggage or visa claims',
	
);


// Check suspiciously long train journeys
// Most return UK journeys should be under 2500km return.
//The world's longest train journeys are all above 3000km
// - there are only 50 of those, mostly in Russia and China.
$database_update =  mysqli_query($connectionmanager->connection,"UPDATE journeys
																SET status = 600
																WHERE mode_of_transport Like 'rail'
																AND distance_km_processed > 2800
																AND status IS NOT NULL
																AND status != 0
																AND status < 600"
										);

if ($database_update === false) {
	echo '<p>Failed to update long train journeys.</p>';
}


										
// Check suspiciously short air journeys

$database_update =  mysqli_query($connectionmanager->connection,"UPDATE journeys
																SET status = 601
																WHERE mode_of_transport Like 'air'
																AND distance_km_processed < 100
																AND distance_km_processed > 0
																AND status IS NOT NULL
																AND status != 0
																AND status < 600"
										); 
if ($database_update === false) {
	echo '<p>Failed to update short plane journeys.</p>';
}

// Check  if there are luggage of visa claims among the journeys

$database_update =  mysqli_query($connectionmanager->connection,"UPDATE journeys
																SET status = 700
																WHERE mode_of_transport Like 'air'
																AND line_description Like '%bag%'
																OR 	line_description Like '%baggage%'
																OR line_description Like '%luggage%'
																OR line_description Like '%visa%'
																OR claim_notes Like '%bag%'
																OR claim_notes Like '%baggage%'
																OR claim_notes Like '%luggage%'
																OR claim_notes Like '%visa%'
																AND status IS NOT NULL
																AND status != 0
																AND status < 600"
										); 
if ($database_update === false) {
	echo '<p>Failed to update luggage or visa claims.</p>';
}


// Check suspiciously expensive air journeys

$database_update =  mysqli_query($connectionmanager->connection,"UPDATE journeys
																SET status = 603
																WHERE mode_of_transport Like 'air'
																AND cost > 2000
																AND status IS NOT NULL
																AND status != 0
																AND status != 601"
										); 
if ($database_update === false) {
	echo '<p>Failed to update expensive plane journeys.</p>';
}


// Check suspiciously expensive rail journeys

$database_update =  mysqli_query($connectionmanager->connection,"UPDATE journeys
																SET status = 604
																WHERE mode_of_transport Like 'rail'
																AND cost > 400
																AND status IS NOT NULL
																AND status != 0
																AND status != 600"
										); 
if ($database_update === false) {
	echo '<p>Failed to update short plane journeys.</p>';
}






?>
    </div>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
