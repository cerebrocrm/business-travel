<?php
/*

## University of Edinburgh Business Travel Database
   Copyright 2017 The University of Edinburgh
   
   @author Chiara Aquino
   @author Joseph Farthing
*/

?><!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
  <meta charset="utf-8">
  <title>University of Edinburgh | Business Travel Reporting</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
  <div class="container">
    <div class="row">

<?php




/**
 * Connects to backend DB
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
class DBconnect {
	public $connection;
	public function connect() {
		set_time_limit(20000);
		$hostname_contacts = "localhost";
		$database_contacts = "srs_businesstravel"; //The name of the database
		$username_contacts = "chiara"; //The username for the database
		$password_contacts = "gTsv64!5"; // The password for the database
		$this->connection = mysqli_connect($hostname_contacts, $username_contacts, $password_contacts, $database_contacts);
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}


$successful = 0;
$failed = 0;
$errors = array();


// Set the limit
$limit = 5000; // the absolute max number of journeys to calculate

// Set up the database
global $connectionmanager;

// Connect to the database
$connectionmanager = new DBConnect();
$connectionmanager->connect();



// Get the airport entries and process them into an array
global $airport;
$airport = array();

// Load the dept_standardisation table
$airport_data = mysqli_query($connectionmanager->connection,"SELECT * FROM airports WHERE 1");
// Go through the table and save it to the array
while ($airport_factor = mysqli_fetch_assoc($airport_data)) {
	$airport[strtolower($airport_factor['Name'])] = $airport_factor['City'];
}	



echo '<p>Getting the data...</p>';
$current_id = 0;

if ($_GET['id']) {
	$current_id = mysqli_real_escape_string($connectionmanager->connection,$_GET['id']);
}

$last_id = $current_id + $limit;

// Get the data we need to process

// for the future, probably want to process only data for that specific year
// $dataset = mysqli_query($connectionmanager->connection,"SELECT * FROM journeys WHERE mode_of_transport Like 'air' AND year = '2018'");

$dataset = mysqli_query($connectionmanager->connection,"SELECT * FROM journeys WHERE mode_of_transport Like 'air' AND journey_id > $current_id AND journey_id < $last_id ORDER BY journey_id ASC");
echo '<p>Number of rows: '.mysqli_num_rows($dataset).'</p>';	

echo '
<div class="twelve columns">
<table class="u-max-full-width">';



// For every row of the data we run this
$i = 0;
while ($journey = mysqli_fetch_assoc($dataset)) {
	
	$runtime = round(microtime(TRUE) - $_SERVER['REQUEST_TIME_FLOAT'], 6);


	$found_city = null;
	$found_city_origin = null;
	
	// change destinations from airport names to cities
	
	if ($journey['destination'] != '' && $journey['destination'] != null) {
	
	$found_city = array();
	
		if (isset($airport[strtolower($journey['destination'])]) && $airport[strtolower($journey['destination'])] != null) { 
			$found_city['City'] = $airport[strtolower($journey['destination'])];
			echo '<p>'.$journey['destination'].' = '.$found_city['City'].'</p>';
													
													
		} else {

			if($journey['destination_how_calc'] == 'google_search') {
			
				$lat = bcdiv($journey['destination_latitude'], 1, 1);
				$long = bcdiv($journey['destination_longitude'], 1, 1);
			
				$coord = array();
				$coord_data = mysqli_query($connectionmanager->connection,"SELECT * FROM airports WHERE Latitude LIKE '".$lat."%' AND Longitude LIKE '".$long."%' ORDER BY airport_id ASC");
				
				
				while ($row = mysqli_fetch_assoc($coord_data)) {
				$coord[] = $row;
				}
				$found_city['City'] = $coord[0]['City'];
				echo '<p>'.$journey['destination'].' = '.$found_city['City'].'</p>';
			}
		}
	}

	
	//change origins from airport names to cities
	
	if ($journey['origin'] != '' && $journey['origin'] != null) {
	
	$found_city_origin = array();
	
		if (isset($airport[strtolower($journey['origin'])]) && $airport[strtolower($journey['origin'])] != null) { 
			$found_city_origin['City'] = $airport[strtolower($journey['origin'])];
			echo '<p>'.$journey['origin'].' = '.$found_city_origin['City'].'</p>';
													
													
		} else {

			if($journey['origin_how_calc'] == 'google_search') {
			
				$lat = bcdiv($journey['origin_latitude'], 1, 1);
				$long = bcdiv($journey['origin_longitude'], 1, 1);
			
				$coord = array();
				$coord_data = mysqli_query($connectionmanager->connection,"SELECT * FROM airports WHERE Latitude LIKE '".$lat."%' AND Longitude LIKE '".$long."%' ORDER BY airport_id ASC");
				
				
				while ($row = mysqli_fetch_assoc($coord_data)) {
				$coord[] = $row;
				}
				$found_city_origin['City'] = $coord[0]['City'];
				echo '<p>'.$journey['origin'].' = '.$found_city_origin['City'].'</p>';
			}
		}
	}									
		
													
	if ($found_city != null) {
	$try_upload = mysqli_query($connectionmanager->connection, "UPDATE journeys SET destination = '".$found_city['City']."' WHERE journey_id = ".$journey['journey_id']);
		if ( $try_upload === false ){
			$failed++;
			$errors[] = mysqli_error($connectionmanager->connection);
		} else {
			$successful++;
		}
	}
	
		if ($found_city_origin != null) {
	$try_upload = mysqli_query($connectionmanager->connection, "UPDATE journeys SET origin = '".$found_city_origin['City']."' WHERE journey_id = ".$journey['journey_id']);
		if ( $try_upload === false ){
			$failed++;
			$errors[] = mysqli_error($connectionmanager->connection);
		} else {
			$successful++;
		}
	}
	
	
	$i++;												

}



echo '<p>successfully edited '.$successful.' journeys out of '.$limit.'.</p>';
echo '</tbody></table></div>';


if (count($errors) > 0) {
		echo '<h2>Database errors</h2><pre>';
		print_r($errors);
		echo '</pre>';
}

if ($last_id < 18061513) {
echo '

<script type="text/javascript">
	window.location.href = "/fix_airports.php?id='.$last_id.'";
</script>';

} else {

echo 'Finished!';

}

?>

    </div>
  </div>

<!-- End Document
    末末末末末末末末末末末末末末末末末末末末末末末末末  -->
</body>
</html>

