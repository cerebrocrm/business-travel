<?php
/*

Code based on Cerebro CRM, also by me
## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   Copyright 2014 Left Join Ltd.
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/

?><!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>University of Edinburgh | Business Travel Reporting</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
	<link rel="stylesheet" href="css/cerebro.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/skeleton-tabs.css">
  
  <style type="text/css">
    .switch_trend_data {
      margin-right: 1em;
    }
  </style>

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">
	
	<!-- JavaScript
	–––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://unpkg.com/vue/dist/vue.min.js"></script>
	<script src="js/skeleton-tabs.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div>
        <div class="container">

            <div class="sixteen columns">
                <h1>Business Travel Data</h1>

            </div>

        </div><!-- container -->
    </div><!-- header -->

<?php
		 
/**
 * Connects to backend DB
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
class DBconnect {
	public $connection;
	public function connect() {
		set_time_limit(20000);
		$hostname_contacts = "localhost";
		$database_contacts = "srs_businesstravel"; //The name of the database
		$username_contacts = "chiara"; //The username for the database
		$password_contacts = "gTsv64!5"; // The password for the database
		$this->connection = mysqli_connect($hostname_contacts, $username_contacts, $password_contacts, $database_contacts);
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}

// Set up the database
global $connectionmanager;

// Connect to the database
$connectionmanager = new DBConnect();
$connectionmanager->connect();


// Status codes
$status_codes = array(
	'0' => 'New data - still processing',
	
	'100' => 'Calculated using the best possible data',
	'101' => 'Calculated with after converting miles to km',
	'102' => 'Calculated using a fuzzy name match',
	'103' => 'Calculated using a Google Search',
	'104' => 'Calculated after correcting a misspelling',
	'110' => 'Calculated using law of cosines method',
	'120' => 'Calculated using google distance method for rail journeys',

	
	'200' => 'Calculated using averages, not specific conversion factors',
	
	'300' => 'Calculated using Plugging the Gaps method',
	
	'400' => 'Error: Could not calculate for an unknown reason',
	'401' => 'Error: Could not calculate because an origin or desination is missing',
	'402' => 'Error: Could not calculate because an origin or destination is unresolvable - it might be mispelt?',
	'403' => 'Error: Could not calculate because a journey mode of transport is missing',
	'404' => 'Error: Could not calculate because distance is a negative number',
	'500' => 'Error: Could not calculate because spend and distance are missing',
	
	'600' => 'Suspiciously long rail journey',
	'601' => 'Suspiciously short flight',
	'602' => 'Possible duplicate',
	'603' => 'Suspiciously expensive air journeys',
	'604' => 'Suspiciously expensive train journeys',
	'700' => 'Possible luggage or visa claims',

);

// Explaining location codes to user

$location_codes = array(
	'code_uk_station'  => 'Code found in UK railway stations list',
	'airport_iata'   => 'International Airport Code',
	'airport_name'   => 'Found in list of airport names',
	'uk_stations'  => 'Found in list of UK railway station names',
	'google_search'   => 'We searched on Google',
	'fuzzy_geoname'  => 'We found a similar name in the OpenStreetMap global database',
	'geoname_uk'   => 'We found a name in the OpenStreetMap database of UK places',
	'geoname'  => 'We found an exact match in the OpenStreetMap database global database',
	'spelling_correction_airport_name'   => 'After a spelling correction, found in list of airport names',
	'london_stations'   => 'Found in the list of London Underground stations',
	'spelling_correction_fuzzy_geoname'   => 'After a spelling correction, we found a similar name in the OpenStreetMap global database',
	'spelling_correction_airport_iata'   => 'After a spelling correction, International Airport Code'
	
);


$codes_for_query = 1;
if ($_GET['ccl4'] && $_GET['ccl4'] != 'ALL_OF_THEM') {
	// get CCL4 code
	$search_ccl4 = $_GET['ccl4'];
	
	
	// get the codes associated with this CCL4
	$codes_for_query = '';
		$codes = mysqli_query($connectionmanager->connection, "SELECT * FROM cost_centres WHERE CCL4 = '".mysqli_real_escape_string($connectionmanager->connection,$search_ccl4)."' GROUP BY ccl6");
	$i = 0;
	while ($row = mysqli_fetch_assoc($codes)) {
		if ($i == 0) {
			$codes_for_query .= 'ccl6 = '.$row['CCL6'].' ';
		} else {
			$codes_for_query .= 'OR ccl6 = '.$row['CCL6'].' ';
		}
		$i++;
	}
	
	
	$codes_for_query = '('.$codes_for_query.')';
}

// list of possible departments
$departments_array = array();
$departments = mysqli_query($connectionmanager->connection, "SELECT CCN4, CCL4 FROM `cost_centres` WHERE CCN4 != '' GROUP BY CCL4 ORDER BY CCN4 ASC");

while ($row = mysqli_fetch_assoc($departments)) {
	if ($_GET['ccl4'] == $row['CCL4']) {
		$row['selected'] = 'selected';
	}
	$departments_array[] = $row;
}

//year	

$years_for_query = '';
if ($_GET['year'] && $_GET['year'] != 'ALL_YEARS') {
	// get CCL4 code
	if ($codes_for_query == 1) {
		$codes_for_query = '';
		$years_for_query = '( year = "'.mysqli_real_escape_string($connectionmanager->connection,$_GET['year']).'")';
	} else {
		$years_for_query = ' AND ( year = "'.mysqli_real_escape_string($connectionmanager->connection,$_GET['year']).'")';
	}
}	

// stats per year

$modes_for_query = '';

if (isset($_GET['show_only'])) {
	if ($_GET['show_only'] != 'ALL_MODES') {
		if ($codes_for_query == 1 && $years_for_query == '') {
			$codes_for_query = '';
			$modes_for_query = " mode_of_transport Like '".mysqli_real_escape_string($connectionmanager->connection,$_GET['show_only'])."' ";
		} else {
			$v = " AND mode_of_transport Like '".mysqli_real_escape_string($connectionmanager->connection,$_GET['show_only'])."' ";
		}
	}
}
	

$stats_year_array = array ();
$stats_year = mysqli_query($connectionmanager->connection,"SELECT mode_of_transport,submode_of_transport,year,
											COUNT(*) as total_count,
											SUM(kg_co2e) as total_co2e,
											SUM(wtt_kg_co2e) as total_wtt_co2e,
											SUM(distance_km_processed) as total_distance_km_processed,
											SUM(cost) as total_cost
											FROM journeys
											WHERE ".$codes_for_query.$modes_for_query."
											GROUP BY mode_of_transport,submode_of_transport,year
											ORDER BY year ASC
											"
										);



//@todo need to fix it for total distance from eExpenses


while ($row = mysqli_fetch_assoc($stats_year)) {

	if ($row['submode_of_transport'] != '') {
		$stats_year_array[$row['year']][$row['submode_of_transport']] = array('total_co2e' => $row['total_co2e']/1000, 'total_wtt_co2e' => $row['total_wtt_co2e'], 'total_distance_km_processed' => $row['total_distance_km_processed'], 'total_cost' => $row['total_cost'], 'total_count' => $row['total_count']);
	} else {
		$stats_year_array[$row['year']][$row['mode_of_transport']] = array('total_co2e' => $row['total_co2e']/1000, 'total_wtt_co2e' => $row['total_wtt_co2e'], 'total_distance_km_processed' => $row['total_distance_km_processed'], 'total_cost' => $row['total_cost'], 'total_count' => $row['total_count']);
	}


}






// get the year array

$years_array = array ();
$years = mysqli_query($connectionmanager->connection, "SELECT year FROM journeys WHERE 1 GROUP BY year DESC");

while ($row = mysqli_fetch_assoc($years)) {
	if ($_GET['year'] == $row['year']) {
		$row['selected'] = 'selected';
	}
	$years_array[] = $row;
}





//

$database_stats =  mysqli_query($connectionmanager->connection,"SELECT
											status, COUNT(*) as count
											FROM journeys
											WHERE ".$codes_for_query.$years_for_query.$modes_for_query."
											GROUP BY status"
										);
$friendly_database_stats = array();
while ($row = mysqli_fetch_assoc($database_stats)) {
	if ($row['status'] == '') {
		$row['status'] = 0;
	}
	$friendly_database_stats[] = array(
		'description' => $status_codes[$row['status']],
		'count' => $row['count']
		);
}


$type_stats =  mysqli_query($connectionmanager->connection,"SELECT
											origin_how_calc, COUNT(*) as count
											FROM journeys
											WHERE ".$codes_for_query.$years_for_query.$modes_for_query."
											GROUP BY origin_how_calc
											ORDER BY count DESC"
										);

$friendly_type_stats = array();

while ($row = mysqli_fetch_assoc($type_stats)) {
	$friendly_type_stats[$location_codes[$row['origin_how_calc']]] = $row['count'];
}

$type_stats =  mysqli_query($connectionmanager->connection,"SELECT
											destination_how_calc, COUNT(*) as count
											FROM journeys
											WHERE ".$codes_for_query.$years_for_query.$modes_for_query."
											GROUP BY destination_how_calc"
										);


while ($row = mysqli_fetch_assoc($type_stats)) {
	$friendly_type_stats[$location_codes[$row['destination_how_calc']]] = $friendly_type_stats[$row['destination_how_calc']]['count']+$row['count'];
}

arsort($friendly_type_stats);

$really_friendly_type_stats = array();

foreach ($friendly_type_stats as $index => $row) {
		if ($index == '') {
			$index = 'No location  given, or it could not be found';
		}
		$really_friendly_type_stats[] = array('description' => $index, 'count' => $row, 'code_number' => $index);
}

$stats = mysqli_query($connectionmanager->connection,"SELECT provider,mode_of_transport,
											SUM(kg_co2e) as total_co2e,
											SUM(wtt_kg_co2e) as total_wtt_co2e,
											SUM(distance_km_processed) as total_distance_km_processed
											FROM journeys
											WHERE ".$codes_for_query.$years_for_query.$modes_for_query."
											GROUP BY mode_of_transport,provider
											"
										);
$friendly_stats = array();
$friendly_mode_stats = array();



// add the missing km from plugging the gaps to air journeys with eExpenses

if ($codes_for_query != 1) {
	$new_code = 'AND '.$codes_for_query.$years_for_query.$modes_for_query;
} elseif ($years_for_query != '') {
	$new_code = ''.$years_for_query.$modes_for_query;
} else {
	$new_code = ''.$modes_for_query;
}
$tot_cost = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT
											SUM(cost) as sum_cost
											FROM journeys
											WHERE provider Like 'eExpenses'
											AND mode_of_transport Like 'Air'
											AND status != 300
											".$new_code."
											"

										));



$tot_km = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT
											SUM(distance_km_processed) as sum_km
											FROM journeys
											WHERE provider Like 'eExpenses'
											AND mode_of_transport Like 'Air'
											AND status != 300
											".$new_code."
											"
										));

if ($tot_cost['sum_cost'] > 0) {
	$spend_factor = $tot_km['sum_km']/$tot_cost['sum_cost'];
}

$ptg_cost = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT
											SUM(cost) as ptg_cost
											FROM journeys
											WHERE provider Like 'eExpenses'
											AND mode_of_transport Like 'Air'
											AND status = 300 
											".$new_code."
											"
										));


$missing_km = $ptg_cost['ptg_cost']*$spend_factor;

while ($row = mysqli_fetch_assoc($stats)) {

	if (!isset($friendly_stats[$row['provider']])) {
		$friendly_stats[$row['provider']] = array();
	}

	if (strtolower($row['mode_of_transport']) == 'air' && strtolower($row['provider']) == 'eexpenses') {
		$row['total_distance_km_processed'] = $row['total_distance_km_processed'] + $missing_km;
	}
	$friendly_stats[$row['provider']][$row['mode_of_transport']] = $row;

	if ($row['mode_of_transport'] != 'air') {
		if (!isset($friendly_mode_stats[$row['mode_of_transport']])) {
			$friendly_mode_stats[$row['mode_of_transport']] = $row['total_co2e'];
		} else {
			$friendly_mode_stats[$row['mode_of_transport']] = ($row['total_co2e'] + $friendly_mode_stats[$row['mode_of_transport']]);
		}
	}


}






$air_stats = mysqli_query($connectionmanager->connection,"SELECT submode_of_transport,
											SUM(kg_co2e) as total_co2e,
											SUM(wtt_kg_co2e) as total_wtt_co2e,
											SUM(distance_km_processed) as total_distance_km_processed
											FROM journeys
											WHERE mode_of_transport Like 'air' 
											".$new_code."
											GROUP BY submode_of_transport
											"
										);

while ($row = mysqli_fetch_assoc($air_stats)) {

	if (!isset($friendly_mode_stats[$row['submode_of_transport']])) {
		$friendly_mode_stats[$row['submode_of_transport']] = $row['total_co2e'];
	} else {
		$friendly_mode_stats[$row['submode_of_transport']] = ($row['total_co2e'] + $friendly_mode_stats[$row['submode_of_transport']]);
	}

}


$friendly_destination_stats = array();
$destination_stats = mysqli_query($connectionmanager->connection, "SELECT destination, COUNT(*) as total_journeys,SUM(kg_co2e) AS total_co2e, SUM(cost) as total_cost, SUM(distance_km_processed) as total_distance_km_processed FROM `journeys` WHERE destination > '' AND destination != '0' ".$new_code." GROUP BY destination ORDER BY total_co2e DESC LIMIT 20");



while ($row = mysqli_fetch_assoc($destination_stats)) {
	$friendly_destination_stats[] = $row;
}




	// all stats
$total_stats = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT
											SUM(kg_co2e) as total_co2e,
											SUM(wtt_kg_co2e) as total_wtt_co2e,
											SUM(distance_km_processed) as total_distance_km_processed,
											SUM(cost) as total_cost,
											COUNT(*) as total_journeys
											FROM journeys
											WHERE ".$codes_for_query.$years_for_query.$modes_for_query."
											"

											));



$total_stats['total_distance_km_processed'] = $total_stats['total_distance_km_processed'] + $missing_km;




?>
 <div class="container" id="app">
		<form method="get" action="" id="search_form">
			<div class="u-pull-right">

					<select name="ccl4" id="ccl4_search">
						<option value="ALL_OF_THEM">All departments</option>
						<option v-for="department in departments" :value="department.CCL4" :selected="department.selected">{{ department.CCN4 }} </option>
					</select>


			</div>

			<div class="u-pull-up">

					<select name="year" id="year_search">
						<option value="ALL_YEARS">All years</option>
						<option v-for="i in years" :value="i.year" :selected="i.selected">{{ i.year }} </option>
					</select>


			</div>

		</form>

			<ul class="tab-nav">
				<li>
					<a class="button active" href="#results">Results</a>
				</li>
				<li>
					<a class="button" href="#statuses">Progress</a>
				</li>
				<li>
					<a class="button" href="#providers">Providers</a>
				</li>

				<li>
					<a class="button" href="#location_types">Locations</a>
				</li>

				<li>
					<a class="button" href="#map">Map</a>
				</li>

				<li>
					<a class="button" href="#trend">Trends</a>
				</li>
			</ul>



			<div class="tab-content">
				<div class="tab-pane active" id="results">
					<div id="donutchart" class="sixteen columns" style="height: 70vh;"></div>
						<div class="four columns ">
							<p><span class="big_number"> {{ Math.round(Number(totals.total_co2e)).toLocaleString('en-GB') }} </span> kg CO<sub>2</sub>e</p>
						</div>
						<div class="four columns">
							<p><span class="big_number"> {{ Math.round(Number(totals.total_distance_km_processed)).toLocaleString('en-GB') }} </span> km</p>
						</div>
						<div class="three columns">
							<p><span class="big_number"> £{{ Number(totals.total_cost).toLocaleString('en-GB') }} </span> cost</p>
						</div>
				</div>

				<div class="tab-pane" id="statuses">
					<div class="row primary_item" v-for="(code,index) in status_codes" :class="{ even: index % 2 == 1 }">
						<div class="five columns"> {{ code.description }}</div>
						<div class="three columns action_bar"><span class="big_number"> {{ Number(code.count).toLocaleString() }} </span> <a class="button borderless open_thread_995" href="#" id="note_0"><span class="fa fa-chevron-down" id="note_0_b1" title="Open"></span></a></div>
					</div>
				</div>

				<div class="tab-pane " id="providers">
					<div v-for="(provider,index) in providers">
						<div class="row primary_item" v-for="(mode,index) in provider" :class="{ even: index % 2 == 1 }">
							<div class="three columns"> {{ mode.provider }} - {{ mode.mode_of_transport }}</div>
							<div class="three columns"> <span class="big_number"> {{ Math.round(Number(mode.total_co2e)).toLocaleString() }} </span> kg CO<sub>2</sub>e </div>
							<div class="three columns"> <span class="big_number"> {{ Math.round(Number(mode.total_distance_km_processed)).toLocaleString() }} </span> km </div>
							<div class="one columns action_bar"> <a class="button borderless open_thread_995" href="#" id="note_0"><span class="fa fa-chevron-down" id="note_0_b1" title="Open"></span></a></div>
						</div>
					</div>
				</div>

				<div class="tab-pane " id="location_types">
					<h2>Top 20 destinations</h2>
					<div class="row primary_item" v-for="(destination,index) in destinations" :class="{ even: index % 2 == 1 }">
						<div class="three columns"> {{ destination.destination.replace(new RegExp("\\\\", "g"), "") }}</div>
							<div class="three columns"> <span class="big_number"> {{ Math.round(Number(destination.total_co2e)).toLocaleString() }} </span> kg CO<sub>2</sub>e </div>
							<div class="three columns"> <span class="big_number"> £{{ Number(destination.total_cost).toLocaleString() }} </span> cost </div>
							<div class="two columns"> <span class="big_number"> {{ Math.round(Number(destination.total_journeys)).toLocaleString() }} </span> journeys </div>

							<div class="one columns action_bar"> <a class="button borderless open_thread_995" href="#" id="note_0"><span class="fa fa-chevron-down" id="note_0_b1" title="Open"></span></a></div>
					</div>

					<p> <br /></p>
					<h2>How locations were found</h2>
					<div class="row primary_item" v-for="(type,index) in location_types" :class="{ even: index % 2 == 1 }">
						<div class="seven columns"> {{ type.description }}</div>
						<div class="three columns action_bar"><span class="big_number"> {{ Number(type.count).toLocaleString() }} </span> <a class="button borderless open_thread_995" href="#" id="note_0"><span class="fa fa-chevron-down" id="note_0_b1" title="Open"></span></a></div>
					</div>
				</div>


				<div class="tab-pane" id="map">
					<iframe src="./map.php?hide_depts&ccl4=<?php echo $_GET['ccl4']; ?>&year=<?php echo $_GET['year']; ?>" style="width: 100%; height: 70vh; border: none;"></iframe>
				</div>


				<div class="tab-pane" id="trend">
					<div id="trend_chart" class="sixteen columns" style="height: 70vh;"></div>
          <div class="eight columns offset-by-one">
            <a href="#" class="button switch_trend_data u-pull-left button-primary" data-show="data">Carbon emissions</a>
            <a href="#" class="button switch_trend_data u-pull-left" data-show="data_cost">Cost</a>
            <a href="#" class="button switch_trend_data u-pull-left" data-show="data_distance">Distance</a>
			<a href="#" class="button switch_trend_data u-pull-left" data-show="data_count">Count</a>
          </div>
				</div>


		</div>


 </div>


<script type="text/javascript">



	var app = new Vue({
		el: '#app',
		data: {
			status_codes: <?php echo json_encode($friendly_database_stats); ?>,
			providers: <?php echo json_encode($friendly_stats); ?>,
			location_types: <?php echo json_encode($really_friendly_type_stats); ?>,
			departments: <?php echo json_encode($departments_array); ?>,
			totals: <?php echo json_encode($total_stats); ?>,
			destinations: <?php echo json_encode($friendly_destination_stats); ?>,
			years: <?php echo json_encode($years_array); ?>
		}
	})



	google.charts.load("current", {packages:["corechart"]});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {
		var data = google.visualization.arrayToDataTable([
			['Mode', 'Kg CO<sub>2</sub>e'],
			['Long-haul flights',     <?php echo ($friendly_mode_stats['international business'] + $friendly_mode_stats['international economy'] + $friendly_mode_stats['international first'] + $friendly_mode_stats['international premium economy'] + $friendly_mode_stats['long average'] + $friendly_mode_stats['long business'] + $friendly_mode_stats['long economy'] + $friendly_mode_stats['long premium economy'] )*1; ?>],
			['Short-haul flights',      <?php echo ($friendly_mode_stats['short average'] + $friendly_mode_stats['short business'] + $friendly_mode_stats['short economy'] + $friendly_mode_stats['short premium economy'])*1; ?>],
			['Domestic flights',  <?php echo ($friendly_mode_stats['domestic average'] )*1; ?>],
			['Road', <?php echo ($friendly_mode_stats['bus'] + $friendly_mode_stats['car'] + $friendly_mode_stats['mileage'] + $friendly_mode_stats['coach'] + $friendly_mode_stats['taxi'])*1; ?>],
			['Rail',    <?php echo ($friendly_mode_stats['rail'])*1; ?>],
			['Ferry',    <?php echo ($friendly_mode_stats['ferry'])*1; ?>],
		]);

		var options = {
			title: 'Business travel emissions',
			pieHole: 0.6,
			colors:['#9AC1D5','#C4D3E0','#3A7790','#9C9C36','#CB6A64', '#7864CB'],
			legend: {
					position: 'labeled'
			},
			pieSliceText: 'none'
		};

		var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
		chart.draw(data, options);
	}



      google.charts.setOnLoadCallback(drawYearChart);

			$(document).ready(function() {

			});

      function drawYearChart(choosedata = 'data') {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
					['Year', 'Long-haul flights', 	'Short-haul flights', 'Domestic flights',  'Road', 'Rail', 'Ferry'],
					<?php
					foreach ($stats_year_array as $year => $year_bar) {

						echo "['$year', ".($year_bar['international business']['total_co2e'] + $year_bar['international economy']['total_co2e'] + $year_bar['international first']['total_co2e'] + $year_bar['international premium economy']['total_co2e'] + $year_bar['long average']['total_co2e'] + $year_bar['long business']['total_co2e'] + $year_bar['long economy']['total_co2e'] + $year_bar['long premium economy']['total_co2e'] )*1 .", ".($year_bar['short average']['total_co2e'] + $year_bar['short business']['total_co2e'] + $year_bar['short economy']['total_co2e'] + $year_bar['short premium economy']['total_co2e'])*1 .", ".($year_bar['domestic average']['total_co2e'])*1 .", ". ($year_bar['bus']['total_co2e'] + $year_bar['mileage']['total_co2e'] + $year_bar['car']['total_co2e'] + $year_bar['coach']['total_co2e'] + $year_bar['taxi']['total_co2e'])*1 .", ".($year_bar['national rail']['total_co2e'] + $year_bar['international rail']['total_co2e'])*1 .", ". ($year_bar['average ferry']['total_co2e'])*1 ."],";
					}
					?>
				]);

        var data_cost = google.visualization.arrayToDataTable([
					['Year', 'Long-haul flights', 	'Short-haul flights', 'Domestic flights',  'Road', 'Rail', 'Ferry'],
					<?php
					foreach ($stats_year_array as $year => $year_bar) {
						echo "['$year', ".($year_bar['international business']['total_cost'] + $year_bar['international economy']['total_cost'] + $year_bar['international first']['total_cost'] + $year_bar['international premium economy']['total_cost'] + $year_bar['long average']['total_cost'] + $year_bar['long business']['total_cost'] + $year_bar['long economy']['total_cost'] + $year_bar['long premium economy']['total_cost'] )*1 .", ".($year_bar['short average']['total_cost'] + $year_bar['short business']['total_cost'] + $year_bar['short economy']['total_cost'] + $year_bar['short premium economy']['total_cost'])*1 .", ".($year_bar['domestic average']['total_cost'])*1 .", ". ($year_bar['bus']['total_cost'] + $year_bar['mileage']['total_cost'] + $year_bar['car']['total_cost'] + $year_bar['coach']['total_cost'] + $year_bar['taxi']['total_cost'])*1 .", ".($year_bar['national rail']['total_cost'] + $year_bar['international rail']['total_cost'])*1 .", ". ($year_bar['average ferry']['total_cost'])*1 ."],";
					}
					?>
				]);


        var data_distance = google.visualization.arrayToDataTable([
					['Year', 'Long-haul flights', 	'Short-haul flights', 'Domestic flights',  'Road', 'Rail', 'Ferry'],
					<?php
					foreach ($stats_year_array as $year => $year_bar) {
						echo "['$year', ".($year_bar['international business']['total_distance_km_processed'] + $year_bar['international economy']['total_distance_km_processed'] + $year_bar['international first']['total_distance_km_processed'] + $year_bar['international premium economy']['total_distance_km_processed'] + $year_bar['long average']['total_distance_km_processed'] + $year_bar['long business']['total_distance_km_processed'] + $year_bar['long economy']['total_distance_km_processed'] + $year_bar['long premium economy']['total_distance_km_processed'] )*1 .", ".($year_bar['short average']['total_distance_km_processed'] + $year_bar['short business']['total_distance_km_processed'] + $year_bar['short economy']['total_distance_km_processed'] + $year_bar['short premium economy']['total_distance_km_processed'])*1 .", ".($year_bar['domestic average']['total_distance_km_processed'])*1 .", ". ($year_bar['bus']['total_distance_km_processed'] + $year_bar['mileage']['total_distance_km_processed'] + $year_bar['car']['total_distance_km_processed'] + $year_bar['coach']['total_distance_km_processed'] + $year_bar['taxi']['total_distance_km_processed'])*1 .", ".($year_bar['national rail']['total_distance_km_processed'] + $year_bar['international rail']['total_distance_km_processed'])*1 .", ". ($year_bar['average ferry']['total_distance_km_processed'])*1 ."],";
					}
					?>									
				]);
				
				
				
		 var data_count = google.visualization.arrayToDataTable([
					['Year', 'Long-haul flights', 	'Short-haul flights', 'Domestic flights',  'Road', 'Rail', 'Ferry'],
					<?php 
					foreach ($stats_year_array as $year => $year_bar) {
						echo "['$year', ".($year_bar['international business']['total_count'] + $year_bar['international economy']['total_count'] + $year_bar['international first']['total_count'] + $year_bar['international premium economy']['total_count'] + $year_bar['long average']['total_count'] + $year_bar['long business']['total_count'] + $year_bar['long economy']['total_count'] + $year_bar['long premium economy']['total_count'] )*1 .", ".($year_bar['short average']['total_count'] + $year_bar['short business']['total_count'] + $year_bar['short economy']['total_count'] + $year_bar['short premium economy']['total_count'])*1 .", ".($year_bar['domestic average']['total_count'])*1 .", ". ($year_bar['bus']['total_count'] + $year_bar['mileage']['total_count'] + $year_bar['car']['total_count'] + $year_bar['coach']['total_count'] + $year_bar['taxi']['total_count'])*1 .", ".($year_bar['national rail']['total_count'] + $year_bar['international rail']['total_count'])*1 .", ". ($year_bar['average ferry']['total_count'])*1 ."],";
					}
					?>									
				]);		

    var options = {
			title: '',
			titleTextStyle: {
				fontSize: 20, // 12, 18 whatever you want (don't specify px)
			},
      seriesType: 'bars',
	  isStacked: true, 
	  colors:['#9AC1D5','#C4D3E0','#3A7790','#9C9C36','#CB6A64', '#7864CB'],
	  backgroundColor: '#ffffff',
	  vAxis: {title: 'Tonnes CO2e'},

    };
	
	
    var chart = new google.visualization.ComboChart(document.getElementById('trend_chart'));
    if (choosedata == 'data_distance') {
       options['title'] = 'Travel distance by year';
       options['vAxis'] =  {title: 'Distance (km)'};
       chart.draw(data_distance, options);
    } else if (choosedata =='data_cost') {
      options['title'] = 'Travel cost by year';
      options['vAxis'] =  {title: '£ (GBP)'};
       chart.draw(data_cost, options);
	 } else if (choosedata =='data_count') {
      options['title'] = 'Travel journeys by year';
      options['vAxis'] =  {title: 'No. of journeys'};
       chart.draw(data_count, options);      
    } else {
      options['title'] = 'Travel emissions by year';
      options['vAxis'] =  {title: 'Tonnes CO2e'};
      chart.draw(data, options);
    }
    
  }
	
	
	$( document ).ready(function() {
		$("#ccl4_search").change(function(e) {
			$("#search_form").submit();
		});
	
		$("#year_search").change(function(e) {
			$("#search_form").submit();
		});
    
    $(window).resize(function() {
      drawChart();
      drawYearChart('data');
    });
    
    $(".tab-nav a").click(function() {
      drawChart();
      drawYearChart();
    });
    
    $(".switch_trend_data").click(function(e) {
      $(".switch_trend_data").removeClass('button-primary');
      $(this).addClass('button-primary');
      e.preventDefault();
      drawYearChart($(this).data('show'));
    });
});
	
</script>

 

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
