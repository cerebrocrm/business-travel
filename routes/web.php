<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


/* Main user interface pages */

$router->get('/','ViewController@DisplayView');

$router->get('/embed/{view}','ViewController@DisplayViewEmbed');

$router->get('/year/{year}[/{view}]','ViewController@DisplayView');

$router->get('/year/{year}/journeys/by-{group_by}/{group_by_var}','ViewController@DisplayView');

$router->get('/journey/{id}','ViewController@DisplayView');


// Upload

$router->get('/upload', function () use ($router) {
    return 'This would be the upload page. '.$router->app->version();
});

// Test
$router->get('/test', 'JourneyController@GetJourneyBlock');


// Functions to look up pieces of data
$router->get('/lookup/status/{status_code}','JourneyProcessor@StatusLookup');
$router->get('/lookup/statuses','JourneyProcessor@Statuses');

$router->get('/lookup/place/{search}','JourneyProcessor@PlaceLookup');
$router->get('/lookup/place/{search}/{mode}','JourneyProcessor@PlaceLookup');
$router->get('/lookup/google/{search}','JourneyProcessor@GoogleLookup');

$router->get('/lookup/departments/','ReportController@Departments');


/* UI FUNCTIONS */
/* Logical journey requests we might need for UI */

// Get journeys by status code
$router->get('/journeys/by-status/{status}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-status/{status}','JourneyController@SimpleJourneyQuery');


// Get a single journey by id
$router->get('/journeys/by-id/{journey_id}','JourneyController@GetJourney');

// Get a single journey by ccl4 code @todo (NOT WORKING AT PRESENT)
$router->get('/journeys/by-ccl4/{journey_id}','JourneyController@GetJourney');

// Get a single journey by ccl6 code
$router->get('/journeys/by-ccl6/{ccl6}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-ccl6/{ccl6}','JourneyController@SimpleJourneyQuery');

// Get a single journey by mode of transport
$router->get('/journeys/by-mode/{mode}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-mode/{mode}','JourneyController@SimpleJourneyQuery');

// Get a single journey by submode of transport
$router->get('/journeys/by-submode/{submode}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-submode/{submode}','JourneyController@SimpleJourneyQuery');

// Get a single journey by origin 
$router->get('/journeys/by-origin/{origin}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-origin/{origin}','JourneyController@SimpleJourneyQuery');

// Get a single journey by destination
$router->get('/journeys/by-destination/{destination}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-destination/{destination}','JourneyController@SimpleJourneyQuery');

// Get a single journey by origin_how_calc
$router->get('/journeys/by-origin_how_calc/{origin_how_calc}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-origin_how_calc/{origin_how_calc}','JourneyController@SimpleJourneyQuery');

// Get a single journey by destination_how_calc
$router->get('/journeys/by-destination_how_calc/{destination_how_calc}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-destination_how_calc/{destination_how_calc}','JourneyController@SimpleJourneyQuery');

// Get a single journey by provider
$router->get('/journeys/by-provider/{provider}','JourneyController@SimpleJourneyQuery');
$router->get('/journeys/year/{year}/by-provider/{provider}','JourneyController@SimpleJourneyQuery');

// All per year data
$router->get('/journeys/year/{year}','JourneyController@SimpleJourneyQuery');


/* REPORTS */

// By mode
$router->get('/report/by-mode','ReportController@JourneysByMode');
$router->get('/report/year/{year}/by-mode','ReportController@JourneysByMode');

// By destination
$router->get('/report/by-destination','ReportController@JourneysByDestination');
$router->get('/report/year/{year}/by-destination','ReportController@JourneysByDestination');

// By destination (minus London leg)
$router->get('/report/by-destination-minus-connections','ReportController@JourneysByDestinationMinusLondonLeg');
$router->get('/report/year/{year}/by-destination-minus-connections','ReportController@JourneysByDestinationMinusLondonLeg');

// By provider
$router->get('/report/by-provider','ReportController@JourneysByDestination');
$router->get('/report/year/{year}/by-provider','ReportController@JourneysByProvider');

// By provider
$router->get('/report/london','ReportController@LondonReport');
$router->get('/report/year/{year}/london','ReportController@LondonReport');


// Generic group report
$router->get('/report/by-{group_by}','ReportController@GroupedJourneyReport');
$router->get('/report/year/{year}/by-{group_by}','ReportController@GroupedJourneyReport');

/* BACKEND FUNCTIONS */

// Process journeys
$router->get('/process/journeys','JourneyProcessor@ProcessJourneyBlock');

$router->get('/process/status','JourneyProcessor@CurrentStatus');


// Recieve an upload
$router->post('/process/upload', function () use ($router) {
    return 'This would where files are uploaded. '.$router->app->version();
});
