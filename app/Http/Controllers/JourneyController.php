<?php

/*
 *  This file is part of The University of Edinburgh
 *  Business Travel Reporting System.
 *
 *  Copyright 2017, 2018 University of Edinburgh
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class JourneyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Start the database connection
        global $select;
        $select = array(
            'journey_id',
            'year',
            'mode_of_transport',
            'submode_of_transport',
            'provider',
            'eexpenses_type',
            'department',
            'cost_centre',
            'ccl6',
            'distance_miles',
            'distance_km',
            'distance_km_processed',
            'start_date',
            'end_date',
            'hours_driven',
            'days_driven',
            'cost',
            'seats',
            'is_return',
            'origin',
            'destination',
            'origin_original',
            'origin_how_calc',
            'destination_original',
            'destination_how_calc',
            'origin_country',
            'destination_country',
            'flight_type',
            'flight_class',
            'origin_latitude',
            'origin_longitude',
            'destination_latitude',
            'destination_longitude',
            'kg_co2e',
            'wtt_kg_co2e',
            'status',
            'last_updated');
        
        
        // department filtering
        
        global $dept_array;
        $dept_array == array();
        
        global $search_department;
        $search_department = '';
        
        if (isset($_GET['dept'])) {
            if ($_GET['dept'] && $_GET['dept'] != 'ALL_OF_THEM' && $_GET['dept'] != '') {
                // get CCL4 code
                $search_department = $_GET['dept'];
            
                // get the codes associated with this department
                $codes = \DB::table('cost_centres')->where('CCL4','like',$search_department)->orWhere('CCL5','like',$search_department)->orWhere('CCL6','like',$search_department)->orWhere('CCL3','like',$search_department)->orWhere('CCL2','like',$search_department)->groupBy('ccl6')->get();
                
                if (count ($codes) > 0) {
                    foreach ($codes as $row) {
                        $dept_array[] = $row->CCL6;
                    }
                }
                
    
            }
            
            
        }
    }
    
        
    
    /**
     * Load journeys from the database
     *
     * @param int $limit
     *   [optional] Total number of journeys to return, defaults to 5000
     *
     * @return array
     */
    public function GetJourneyBlock($limit = 5000) {
        $results = (array) \DB::select('SELECT * FROM journeys WHERE status = 0 LIMIT :limit', ['limit' => $limit]);
        return $results;
    }
    
    /**
     * Load a single journey
     *
     * @return $journey array
     */
    public function GetJourney($journey_id) {
        global $select;
        $return = DB::table('journeys')
        ->select($select)
        ->where('journey_id', '=', $journey_id)->get();
        
        return $return;

    }
    
    /**
     * Save a single journey
     *
     * @return $journey array
     */
    public function SaveJourney($journey) {
        
        // Update the data
        $result = DB::table('journeys')
            ->where('journey_id', $journey['journey_id'])
            ->update($journey);
            
        return $result;
    }
    
    /**
     * A sophisticated query of the journey table using the Laravel query builder
     *
     * @return $journey array
     */
    public function JourneyQuery($query, $order_by = array('journey_id', 'asc'), $group_by = 'journey_id') {
        
        /* ROWS TO SHOW
         * journey_id
         * year
         * mode_of_transport
         * submode_of_transport
         * provider
         * eexpenses_type
         * department
         * cost_centre
         * ccl6
         * distance_miles
         * distance_km
         * distance_km_processed
         * start_date
         * end_date
         * hours_driven
         * days_driven
         * cost
         * seats
         * is_return
         * origin
         * destination
         * origin_original
         * origin_how_calc
         * destination_original
         * destination_how_calc
         * origin_country
         * destination_country
         * flight_type
         * flight_class
         * origin_latitude
         * origin_longitude
         * destination_latitude
         * destination_longitude
         * kg_co2e
         * wtt_kg_co2e
         * status
         * last_updated
         */

        
        // We select a limited subset of fields in this mode to not expose any potentially harmful data
        global $select;
        global $dept_array;
            
        if (count($dept_array > 0) && $dept_array != null) {

            $return = \DB::table('journeys')
            ->select($select)
            ->where($query)
            ->whereIn('ccl6', $dept_array)
            ->groupBy($group_by)
            ->orderBy($order_by[0], $order_by[1])
            ->paginate(10);
        } else {
            $return = \DB::table('journeys')
            ->select($select)
            ->where($query)->groupBy($group_by)->orderBy($order_by[0], $order_by[1])->paginate(10);
        }
        
        
        return $return;
    }
    
    
    /**
     * A simple version of the journey query for common searches table using the Laravel query builder
     *
     * @return $journey array
     */
    public function SimpleJourneyQuery($year = null, $mode = null, $submode = null, $origin = null, $destination = null, $origin_how_calc = null, $destination_how_calc = null, $ccl6 = null, $provider = null, $eexpenses_type = null, $status = null) {
        
        if (isset($_GET['order_by']) && isset($_GET['order_dir'])) {
            $order_by = array($_GET['order_by'],$_GET['order_dir']);
        } else {
            $order_by = null;
        }
        
        if (isset($_GET['group_by'])) {
            $group_by = $_GET['group_by'];
        } else {
            $group_by = null;
        }
        
        // Build the query array
        $query = array();
        
        // We go through each variable. If its not null, we add it to the query
        if ($year != null) {
            $query[] = array('year', '=', urldecode($year));
        }
        
        if ($mode != null) {
            $query[] = array('mode_of_transport', 'like', urldecode($mode));
        }
        
        if ($submode != null) {
            $query[] = array('submode_of_transport', 'like', urldecode($submode));
        }
        
        if ($origin != null) {
            $query[] = array('origin', 'like', $origin);
        }
        
        if ($destination != null) {
            $query[] = array('destination', 'like', urldecode($destination));
        }
        
        if ($origin_how_calc != null) {
            $query[] = array('origin_how_calc', '=', urldecode($origin_how_calc));
        }
        
        if ($destination_how_calc != null) {
            $query[] = array('destination_how_calc', '=', urldecode($destination_how_calc));
        }
        
        if ($ccl6 != null) {
            $query[] = array('ccl6', '=', urldecode($ccl6));
        }
        
        if ($provider != null) {
            $query[] = array('provider', 'Like', urldecode($provider));
        }
        
        if ($eexpenses_type != null) {
            $query[] = array('eexpenses_type', '=', urldecode($eexpenses_type));
        }
        
        if ($status != null) {
            $query[] = array('status', '=', urldecode($status));
        }
        
        // Now run the query and return the results
        if (isset($_GET['order_by']) && isset($_GET['order_dir'])) {
            $order_by = array($_GET['order_by'],$_GET['order_dir']);
            return $this->JourneyQuery($query,$order_by);
        } else {
            return $this->JourneyQuery($query);
        }
        
        
        
    }
    

    //
}
