<?php

/*
 *  This file is part of The University of Edinburgh
 *  Business Travel Reporting System.
 *
 *  Copyright 2017, 2018 University of Edinburgh
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

class JourneyProcessor extends JourneyController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Start the database connection
        
        // Set up spelling system
        global $pspell_link;
        $pspell_link = pspell_new("en");
        
        
        // Google API data
        global $googleAPIKey;
        $googleAPIKey = env('GOOGLE_API_KEY', false);
        
        if ($googleAPIKey == false) {
            abort(503,'No Google API Key has been configured');
        }
        
        // Status codes
        global $status_codes;
        $status_codes = array(
            '' => 'New data',
            '0' => 'Data being recalculated',
            '100' => 'Calculated using the best possible data',
            '101' => 'Calculated with after converting miles to km',
            '102' => 'Calculated using a fuzzy name match',
            '103' => 'Calculated using a Google Search',
            '104' => 'Calculated after correcting a misspelling',
            '110' => 'Calculated using law of cosines method',
            '120' => 'Calculated using google distance method for rail journeys',
            
            '200' => 'Calculated using averages, not specific conversion factors',
            
            '300' => 'Calculated using Plugging the Gaps method',
            
            '400' => 'Error: Could not calculate for an unknown reason',
            '401' => 'Error: Could not calculate because an origin or desination is missing',
            '402' => 'Error: Could not calculate because an origin or destination is unresolvable - it might be mispelt?',
            '403' => 'Error: Could not calculate because a journey mode of transport is missing',
            '404' => 'Error: Could not calculate because distance is a negative number',
            '500' => 'Error: Could not calculate because spend and distance are missing',
            '600' => 'Suspiciously long rail journey',
            '601' => 'Suspiciously short flight',
            '602' => 'Possible duplicate',
            '603' => 'Suspiciously expensive air journeys',
            '604' => 'Suspiciously expensive train journeys',
            '700' => 'Possible luggage or visa claims',
        
        );
        
        
        // Get the DEFRA factors and process into an array
        global $defra_factors;
        $defra_factors = array();
        
        // Load the defra factor table
        $defra_data = \DB::select('SELECT * FROM defra_factors WHERE 1');
        
        // Go through the table and save it to the array
        foreach ($defra_data as $factor) {
            $defra_factors[strtolower($factor->Year)][strtolower($factor->Mode)][strtolower($factor->Submode)] = (array) $factor;
        }
        
        
        // Get the WTT DEFRA factors and process into an array
        global $WTT_defra_factors;
        $WTT_defra_factors = array();
        
        // Load the WTT defra factor table
        $WTT_defra_data = \DB::select('SELECT * FROM WTT_defra_factors WHERE 1');
        
        // Go through the table and save it to the array
        foreach ($WTT_defra_data as $WTT_factor) {
            $WTT_defra_factors[strtolower($WTT_factor->Year)][strtolower($WTT_factor->Mode)][strtolower($WTT_factor->Submode)] = (array) $WTT_factor;
        }
        
        
        
        //Get the spend factors for plugging the gaps and process into an array
        global $spend_factors;
        $spend_factors = array();
        
        // Load the spend factor table
        $spend_data = (array) \DB::select('SELECT * FROM spend_factors WHERE 1');
        
        
        // Go through the table and save it to the array
        foreach ($spend_data as $ptg_factor) {
            $ptg_factor = (array) $ptg_factor;
            if ($ptg_factor['subtype'] == null) {
               $ptg_factor['subtype']  = 0;
            }
            $spend_factors[strtolower($ptg_factor['year'])][strtolower($ptg_factor['mode'])][strtolower($ptg_factor['subtype'])] = $ptg_factor;
            
        }
        
        
        //Get the average passenger occupancy data and process into an array
        global $avg_occupancy;
        $avg_occupancy = array();
        
        // Load the table
        $occupancy_data = \DB::select('SELECT * FROM passenger_occupancy WHERE 1');
        
            
        // Go through the table and save it to the array
        foreach ($occupancy_data as $occupancy_factor) {
            $occupancy_factor = (array) $occupancy_factor;
            $avg_occupancy[strtolower($occupancy_factor['mode'])] = $occupancy_factor;
            
        }
        
        // Things to keep statistics
        global $number_google_searches;
        $number_google_searches = 0;
        global $number_calculated_co2;
        $number_calculated_co2 = 0;
        global $number_uncalculated_co2;
        $number_uncalculated_co2 = 0;
        
        $successful = 0;
        $failed = 0;
        $errors = array();
        
    }
    
    /**
    * Return an explanation of what a status code means
    *
    * @return string The explanation
    */
    public function StatusLookup($status_code) {
        global $status_codes;
        return $status_codes[$status_code];
    }
    
    
    /**
    * Return an explanation of what a status code means
    *
    * @return string The explanation
    */
    public function Statuses() {
        global $status_codes;
        return $status_codes;
    }


    /**
    * Calculates the great-circle distance between two points, with
    * the Law of Cosines formula.
    * Based on https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
    * @param float $latitudeFrom Latitude of start point in [deg decimal]
    * @param float $longitudeFrom Longitude of start point in [deg decimal]
    * @param float $latitudeTo Latitude of target point in [deg decimal]
    * @param float $longitudeTo Longitude of target point in [deg decimal]
    * @param float $earthRadius Mean earth radius in [m]
    * @return float Distance between points in [m] (same as earthRadius)
    * 
    */
    public static function SphericalLawOfCosines ($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo); 	
        $lonTo = deg2rad($longitudeTo);
        
        $latDelta = $latFrom - $latTo;
        $lonDelta = $lonFrom - $lonTo;
        
        //$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        //cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        
        $angle	= acos(sin($latFrom) * sin($latTo) + cos($latTo) * cos($latFrom) * cos($lonFrom-$lonTo));
        return ($angle * $earthRadius)/1000;
   }
   
   /**
    * Use Google Place API to find a location
    *
    * @return array Google location data
    */
    
    public function GoogleLookup($search) {
        
        // Array of banned types
        // I've listed all possible types
        // Uncomment a type to remove it from searches
        // Complete list of types available at:
        // https://developers.google.com/places/web-service/supported_types
        $forbidden_types = array(
            'administrative_area_level_1',
            'administrative_area_level_2',
            'administrative_area_level_3',
            'administrative_area_level_4',
            'administrative_area_level_5',
            'colloquial_area',
            //'country',
            //'establishment',
            'finance',
            'floor',
            'food',
            'general_contractor',
            'geocode',
            'health',
            //'intersection',
            //'locality',
            //'natural_feature',
            //'neighborhood',
            //'place_of_worship',
            //'political',
            //'point_of_interest',
            //'post_box',
            'postal_code',
            'postal_code_prefix',
            'postal_code_suffix',
            //'postal_town',
            'premise',
            'room',
            //'route',
            'street_address',
            'street_number',
            //'sublocality',
            //'sublocality_level_4',
            //'sublocality_level_5',
            //'sublocality_level_3',
            //'sublocality_level_2',
            //'sublocality_level_1',
            'store',
            'subpremise',
            'atm'
            );
            
            
        global $number_google_searches;
        $number_google_searches++;
        
        // Google API data
        global $googleAPIKey;
        
        // The search string we're using
        $query = urlencode(preg_replace("/[^A-Za-z ]/", '',$search));
        
        // OK, we're going to do some basic changes to try and prefer EU locations
        $query_europe = $query.',%20Europe';
        
        $data = file_get_contents('https://maps.googleapis.com/maps/api/place/textsearch/json?query='.$query_europe.'&key='.$googleAPIKey);
        
        
        
        $data_encoded = json_decode($data,true)['results'];
        
        //testing print_r($data_encoded);
        
        // Remove forbidden types
        foreach ($data_encoded as $i => $d) {
            // Remove a place result if it is of a forbidden type.
            //
            // There is a very rare edge case where the Google API doesn't return any types
            // for a place so in that case we just have to assume that the place is a forbidden
            // type so that the journey can later be flagged or handled by other parts of Journey
            // Processor.
            if (!isset($d['types']) || count(array_intersect($forbidden_types, $d['types'])) > 0 ) {
                unset($data_encoded[$i]);
                $data_encoded = array_values($data_encoded);
            }
        }
        
        if (isset($data_encoded[0])) {
         return $data_encoded[0];
        } else {
         return null;
        }
        
    
    }
    
    /**
    * Use Google Place API to find a distance between 2 points
    *
    * @return array Distance in km
    */
    public function GoogleDistance($origin_latitude, $origin_longitude, $destination_latitude, $destination_longitude, $mode = 'rail') {
	
        
        global $number_google_searches;
        $number_google_searches++;
        
        // Google API data
        global $googleAPIKey;
        
        
        $data = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='.$origin_latitude.','.$origin_longitude.'&destinations='.$destination_latitude.','.$destination_longitude.'&mode='.$mode.'&key='.$googleAPIKey);
        
        $decode = json_decode($data,true);
        
        if (isset($decode['rows'][0]['elements'][0]['distance']['value'])) {
            $data_encoded = $decode['rows'][0]['elements'][0]['distance']['value'];
            return $data_encoded/1000;
        } else {
            return false;
        }
        
    }

  /**
   * When we query the journeys table for past journeys we only want to return journeys which had a status code
   * below a specific number, to prevent journeys which might not be accurate or had an error from being used.
   * However the mode of travel sometimes determines how strict we want to be when it comes to finding past journeys.
   * In reality for air and rail we don't want to find journeys which previously returned a status code of 300, in
   * other words we don't want to return journeys which used the "Plugging the Gaps" method because the data should
   * be consistent, but for all other journey types such as bus or taxi where the origin and destination data is always
   * much less consistent between journeys, to cut down on cost of using Google's API we are happy to use past journeys
   * where Plugging the Gaps (code 300) method was used.
   *
   * @param string $mode
   *   The mode to use to determine which code to return
   *
   * @return int
   *   The code to use in queries, queries should look like "status < code"
   */
  private function getStatusCodeToCheckBelow($mode) {
    if ($mode == 'air' || $mode == 'rail') {
      return 300;
    } else {
      return 400;
    }
  }

    /**
    * Try to find a place using a variety of methods
    *
    * @global connectionmanager;
    * @return array ('name' => The name of the place, 'latitutde' => number, 'longitude' => number);
    */
    
    public function PlaceLookup($search, $no_google = false, $mode = null) {
        
        if (strlen($search) < 2) {
            return false;
        }

        $search = urldecode($search);
        
        // Let's make an array to do fuzzy matching later
        $possibles = array();
        //$search = mysqli_real_escape_string($connectionmanager->connection,$search);
        
        // first we look at existing origins and destinations

      $status_to_check_below = $this->getStatusCodeToCheckBelow($mode);

        if (isset($mode) && $mode != '') {
           $location_lookup =  (array) \DB::select('SELECT journey_id, origin, origin_latitude, origin_longitude, origin_how_calc, origin_country FROM `journeys` WHERE origin_original = :search AND mode_of_transport = :mode  AND status < :status GROUP BY origin_how_calc ORDER BY LENGTH(origin_latitude) DESC LIMIT 1', ['search' => $search, 'mode' => $mode, 'status' => $status_to_check_below]);
           
           if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
           
           if (isset($location_lookup['origin_latitude']) && isset($location_lookup['origin_longitude'])) {
               return array('name' => $location_lookup['origin'], 'latitude' => $location_lookup['origin_latitude'], 'longitude' => $location_lookup['origin_longitude'], 'type' => 'previous_origin_'.$location_lookup['origin_how_calc'].'_'.$location_lookup['journey_id'], 'country' => $location_lookup['origin_country']);
           }
           
           $location_lookup =  (array) \DB::select('SELECT journey_id, destination, destination_latitude, destination_longitude, destination_how_calc, destination_country FROM `journeys` WHERE destination_original = :search AND mode_of_transport = :mode AND status < :status GROUP BY origin_how_calc ORDER BY LENGTH(origin_latitude) DESC LIMIT 1', ['search' => $search, 'mode' => $mode, 'status' => $status_to_check_below]);
        
           if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
               
           if (isset($location_lookup['destination_latitude']) && isset($location_lookup['destination_longitude'])) {
                  return array('name' => $location_lookup['destination'], 'latitude' => $location_lookup['destination_latitude'], 'longitude' => $location_lookup['destination_longitude'], 'type' => 'previous_destination_'.$location_lookup['destination_how_calc'].'_'.$location_lookup['journey_id'], 'country' => $location_lookup['destination_country']);
           }
        } else {
          $location_lookup =  (array) \DB::select('SELECT journey_id, origin, origin_latitude, origin_longitude, origin_how_calc, origin_country FROM `journeys` WHERE origin_original = :search  AND status < :status GROUP BY origin_how_calc ORDER BY LENGTH(origin_latitude) DESC LIMIT 1', ['search' => $search, 'status' => $status_to_check_below]);
           
           if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
           
           if (isset($location_lookup['origin_latitude']) && isset($location_lookup['origin_longitude'])) {
               return array('name' => $location_lookup['origin'], 'latitude' => $location_lookup['origin_latitude'], 'longitude' => $location_lookup['origin_longitude'], 'type' => 'previous_origin_'.$location_lookup['origin_how_calc'].'_'.$location_lookup['journey_id'], 'country' => $location_lookup['origin_country']);
           }
           
           $location_lookup =  (array) \DB::select('SELECT journey_id, destination, destination_latitude, destination_longitude, destination_how_calc, destination_country FROM `journeys` WHERE destination_original = :search  AND status < :status GROUP BY origin_how_calc ORDER BY LENGTH(origin_latitude) DESC LIMIT 1', ['search' => $search, 'status' => $status_to_check_below]);
        
           if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
               
           if (isset($location_lookup['destination_latitude']) && isset($location_lookup['destination_longitude'])) {
                  return array('name' => $location_lookup['destination'], 'latitude' => $location_lookup['destination_latitude'], 'longitude' => $location_lookup['destination_longitude'], 'type' => 'previous_destination_'.$location_lookup['destination_how_calc'].'_'.$location_lookup['journey_id'], 'country' => $location_lookup['destination_country']);
           }
        }
        
        
        
        
        
        $search = strtolower($search);
        // We fix the most obviously stupid mistake
        if ($search == 'airport' || $search == 'edin' || $search == 'edinb' || $search == 'edinbr'|| $search == 'edinburgh international' || $search == 'ed' || $search == 'edi') {
            $search = 'Edinburgh';	
        }
        
        if ($search == 'city' ) {
            $search = 'LCY';
        }
        
        if ($search == 'kings x' ) {
            $search = 'London Kings Cross';
        }
        
            
        // OK we're going to try table lookups first
            
        if ($mode == 'rail') {
            
            if (strlen($search) == 3 ) {
               
               $location_lookup =  (array) \DB::select('SELECT * FROM railway_stations WHERE StationCode Like :search LIMIT 1', ['search' => $search]);
               if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
                               
               if (isset($location_lookup['Latitude']) && isset($location_lookup['Longitude'])) {
                    return array('name' => $location_lookup['StationName'], 'latitude' => $location_lookup['Latitude'], 'longitude' => $location_lookup['Longitude'], 'type' => 'code_uk_station', 'country' => 'United Kingdom');
                }
            }
                
            $remove_these_characters = array(
                'rail station',
                'railway station',
                'train station',
                'tube station', // added by Joe 2017-12-07
                'station', // station on its own should always be last
            );
            
            $search_for_rail = trim(str_replace($remove_these_characters,'',strtolower($search)));
                
            
            // London stations
            $location_lookup = (array) \DB::select('SELECT * FROM london_stations WHERE station Like :search LIMIT 1', ['search' => $search_for_rail]);
            
            if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
            
            if (isset($location_lookup['latitude']) && isset($location_lookup['longitude'])) {
                return array('name' => $location_lookup['station'], 'latitude' => $location_lookup['latitude'], 'longitude' => $location_lookup['longitude'], 'type' => 'london_stations', 'country' => 'United Kingdom');
            }
            
            // UK stations
            $location_lookup = (array) \DB::select('SELECT * FROM railway_stations WHERE StationName Like :search LIMIT 1', ['search' => $search_for_rail]);
            
            if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
            
            if (isset($location_lookup['Latitude']) && isset($location_lookup['Longitude'])) {
                return array('name' => $location_lookup['StationName'], 'latitude' => $location_lookup['Latitude'], 'longitude' => $location_lookup['Longitude'], 'type' => 'uk_stations', 'country' => 'United Kingdom');
            }
        }
        
        
        
        // First, airport codes
        
        if ($mode == 'air') {
            
            if (strlen($search) == 3) {

                $location_lookup = (array) \DB::select('SELECT * FROM airports WHERE IATA Like :search LIMIT 1', ['search' => $search]);
        
                if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
                
                if (isset($location_lookup['Latitude']) && isset($location_lookup['Longitude'])) {
                    return array('name' => $location_lookup['City'], 'latitude' => $location_lookup['Latitude'], 'longitude' => $location_lookup['Longitude'], 'type' => 'airport_iata', 'country' => $location_lookup['Country']);
                }
            }
            
            
            $remove_these_characters = array(
                'international airport',
                'international',
                'airport',
                'intl'
            );
            
            $search_for_air = trim(str_replace($remove_these_characters,'',strtolower($search)));	
            
            $location_lookup = (array) \DB::select('SELECT * FROM airports WHERE City Like :search ORDER BY airport_id ASC LIMIT 1', ['search' => '%'.$search_for_air.'%']);
            if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};

            if (isset($location_lookup['Latitude']) && isset($location_lookup['Longitude'])) {
                return array('name' => $location_lookup['City'], 'latitude' => $location_lookup['Latitude'], 'longitude' => $location_lookup['Longitude'], 'type' => 'airport_name', 'country' => $location_lookup['Country']);
            }
            
            $location_lookup = (array) \DB::select('SELECT * FROM airports WHERE Name Like :search ORDER BY airport_id ASC LIMIT 1', ['search' => $search_for_air]);
            if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
            
            if (isset($location_lookup['Latitude']) && isset($location_lookup['Longitude'])) {
                return array('name' => $location_lookup['City'], 'latitude' => $location_lookup['Latitude'], 'longitude' => $location_lookup['Longitude'], 'type' => 'airport_city', 'country' => $location_lookup['Country']);
                    
            }
            
            $location_lookup = (array) \DB::select('SELECT * FROM airports WHERE Name Like :search OR City Like :search2 ORDER BY airport_id ASC LIMIT 1', ['search' => '%'.$search_for_air.'%', 'search2' => '%'.$search_for_air.'%']);
            if (isset($location_lookup[0])) {$location_lookup = (array) $location_lookup[0];};
            
            if (isset($location_lookup['Latitude']) && isset($location_lookup['Longitude'])) {
                return array('name' => $location_lookup['City'], 'latitude' => $location_lookup['Latitude'], 'longitude' => $location_lookup['Longitude'], 'type' => 'airport_name', 'country' => $location_lookup['Country']);
            }
        }
    
        // If we don't have an airport, then let's try the geoplaces table
        
        
        // first let's try to see if it's in the UK
        $location_lookup_uk = \DB::select('SELECT * FROM geonames WHERE name Like :search AND country_standard_name LIKE "Europe/London" AND population > 90000', ['search' => $search]);
        
        $location_lookup = \DB::select('SELECT * FROM geonames WHERE name_latin Like :search', ['search' => $search]);
        
        
        if (count($location_lookup_uk) === 1) {
            // We have one result exactly, it's probably the right answer
            
            $return = (array) $location_lookup_uk[0];
            return array('name' => $return['name_latin'], 'latitude' => $return['latitude'], 'longitude' => $return['longitude'], 'type' => 'geoname_uk', 'country' => \Locale::getDisplayRegion('-'.$return['country_code'], 'en'));
        
        
        } elseif (count($location_lookup) === 1) {
            // We have one result exactly, it's probably the right answer
            $return = (array) $location_lookup[0];
            
            return array('name' => $return['name_latin'], 'latitude' => $return['latitude'], 'longitude' => $return['longitude'], 'type' => 'geoname', 'country' => \Locale::getDisplayRegion('-'.$return['country_code'], 'en'));
        
        } else if (count($location_lookup) > 1) {
            // We have more than one precise match
            foreach ($location_lookup as $row) {
                $row = (array) $row;
                $possibles[] = $row;
            }
        } else {
            // This is where we try a more fuzzy match
            $location_lookup2 = \DB::select('SELECT * FROM geonames WHERE name_latin Like :search ORDER BY population DESC', ['search' => '%'.$search.'%']);
            
           foreach ($location_lookup2 as $row) {
                $row = (array) $row;
                $possibles[] = $row;
            }
            
        }
        
        
        if (count($possibles) > 0) {
            // Try to inteligently match the fuzzy data
            
            /* Let's imagine we have a search for York.
             * It would be logical to guess that the largest population place is most likely to be the one we want,
             * but sometimes that would be "New York". So checking for how similar the name is would matter.
             *
             * Even if we exclude wildcards, and only return places called exactly "York", them biggest place
             * called York is in Australia, so we need to prioritise places with country code GB.
             *
             * This code tries to score each place on 3 factors:
             *  - levenshtein difference
             *  - whether the place is in the UK
             *  - population
             */
            
            foreach ($possibles as $i => $p) {
    
                // How similar the name is to the search query
                $similarity_of_name = -1;
                
                similar_text(strtolower($p['name_latin']),strtolower($search), $similarity_of_name);
                
                // Whether its in the UK or Europe
                $is_uk = 0;
                $is_eu = 0;
                if ($p['country_code'] == 'GB') {
                    $is_uk = 1;
                } else if (strpos($p['country_standard_name'],'Europe') !== false) {
                    $is_eu = 1; // UK being separate from EU is a scoring thing, not a comment on Brexit :-)
                }
                
                // How many significant digits the population is
                $population_scale = strlen($p['population']);
                
                $possibles[$i]['similarity'] = $similarity_of_name;
                $possibles[$i]['is_uk'] = $is_uk;
                $possibles[$i]['is_eu'] = $is_eu;
                $possibles[$i]['population_scale'] = $population_scale;
                $possibles[$i]['score'] = $population_scale+$is_uk+$is_eu+$similarity_of_name;
                
            }
            
            // Sort by score
            usort($possibles, function($a, $b) {
                return $a['score'] - $b['score'];
            });
            
            
            $most_likely = array_pop($possibles);
            
            if ($most_likely['similarity'] > 60) {
                return array('name' => $most_likely['name_latin'], 'latitude' => $most_likely['latitude'], 'longitude' => $most_likely['longitude'],'similarity' => $most_likely['similarity'], 'population_magnitude' => $population_scale, 'type' => 'fuzzy_geoname', 'country' => \Locale::getDisplayRegion('-'.$most_likely['country_code'], 'en'),  'score' => $most_likely['score'], 'other_matches' => $possibles,);
            }
            
        }
    
        // No results so far
        // Now we try the sophisticated stuff
        
        // We exclude API calls when google is turned off. This also disables spellchecking to avoid server load problems.	
        if ($no_google == false) {
    
        
            // Try to correct spelling errors using en_GB pspell library
            global $pspell_link;
    
            $spellings = pspell_suggest($pspell_link,$search);
            
            if (isset($spellings[0])) {
               similar_text(strtolower($spellings[0]),strtolower($search), $similarity_of_name);
               
               if ($similarity_of_name > 79) {
                // Run the database check only on the first suggested spelling of the result
                $check_the_mispelling = $this->PlaceLookup($spellings[0],true,$mode);
               }
            }
                        
            
            
            
        
            
            if (isset($check_the_mispelling['name']) && $check_the_mispelling['name'] != '') {
                            
                $check_the_mispelling['type'] = 'spelling_correction_'.$check_the_mispelling['type'];
                $check_the_mispelling['assumed_search'] = $spellings[0];
                $check_the_mispelling['original_query'] = $search;
                $check_the_mispelling['similarity'] = $similarity_of_name;
                return $check_the_mispelling;
            } else {
                            
                // Now we do the google lookup
                $google_lookup = $this->GoogleLookup($search);	
                
                if (isset($google_lookup['geometry']['location']['lat']) && isset($google_lookup['geometry']['location']['lng']) ) {
                    if ($google_lookup['formatted_address']) {
                        $country = substr($google_lookup['formatted_address'], strrpos($google_lookup['formatted_address'], ','));
                        $country = trim(str_replace(', ','',$country));
                    } else {
                        $country = 'UNKNOWN';
                    }
                    return array('name' => $google_lookup['name'], 'latitude' => $google_lookup['geometry']['location']['lat'], 'longitude' => $google_lookup['geometry']['location']['lng'], 'type' => 'google_search', 'types' => $google_lookup['types'],'country' => $country);
                }
                
            }
            
        }
	
    }
    
    
    /**
     * Send a block of journeys for processing, journeys will be retrieved from the database
     * using self::GetJourneyBlock.
     *
     * You can specify various limits by passing them as parameters, the processor will stop when
     * any of the following limits are reached.
     *
     * @param int $limit
     *   [optional] Total number of journeys to process, defaults to 5000
     * @param int $google_limit
     *   [optional] Total number of requests to send to Google's Places API, defaults to 125
     * @param int $time_limit
     *   [optional] Total number of seconds to run for before the processor will stop processing
     *   journeys, defaults to 45
     *
     * @return array
     *   Statistics on the success of the process block
     *
     * @see \App\Http\Controllers\JourneyController::GetJourneyBlock()
     * @see \App\Http\Controllers\JourneyProcessor::ProcessJourney()
     * @see \App\Http\Controllers\JourneyProcessor::SaveJourney()
     */
    public function ProcessJourneyBlock($limit = 5000, $google_limit = 125, $time_limit = 45) {
        
        echo 'Processing a batch of '.$limit,' journeys. Last successful journey will be stored in the process_status table. ';
        // get the journeys
        $journeys = $this->GetJourneyBlock($limit);
        
        // Things to keep statistics on
        global $number_google_searches;
        global $number_calculated_co2;
        global $number_uncalculated_co2;
        
        $successful = 0;
        $failed = 0;
        $errors = array();
        $results = array();
        
        
        foreach ($journeys as $journey) {
            $journey = (array) $journey; // use an array not a stdObj
            
            // Record how long the program has been running
            $runtime = round(microtime(TRUE) - $_SERVER['REQUEST_TIME_FLOAT'], 6);
            // @todo: do something about/with the PHP max script execution time limit, maybe check if this $time_limit is greater and if so lower it

            $note = '';
            
            if ($number_google_searches < $google_limit && $runtime < $time_limit) {
                
                // Process the journey
                $return = $this->ProcessJourney($journey);
                
                print_r($return);
                
                // Save the journey back to the database
                $results[] = $this->SaveJourney((array) $return);
                
                // Update the statistics
                global $number_calculated_co2;
                global $number_uncalculated_co2;
                
                if ($return['kg_co2e'] > 0) {
                    $number_calculated_co2++;
                } else {
                    $number_uncalculated_co2++;
                }
                                
            } else {
               if ($number_google_searches < $google_limit) {
                  $note = 'Reached the limit in Google searches.';
               } else {
                  $note = 'Ran out of time.';
               }
            }
            
        }
        
        if (!isset($note) || $note == '') {
         $note = 'Finished with no major problems.';
        }
        
        if (( $number_calculated_co2 + $number_uncalculated_co2) > 0) {
            \DB::table('process_status')->insert(
               ['number_successful' => $number_calculated_co2,
                'number_failed' => $number_uncalculated_co2,
                'note' => $note
                ]
           );
        }
        

        
        return $results;
        
    }
    

    /**
    * Actually do the work and process the CO2 emissions of a given journey
    *
    * @return array $journey (modified);
    */
    
    public function ProcessJourney($journey ,$status = null) {
        
        // Clean up submode
        if ($journey['submode_of_transport'] == '0') {
            $journey['submode_of_transport'] = null;
        }
        
        // Convert miles to km
        if (($journey['distance_km'] == null || $journey['distance_km'] == 0) && $journey['distance_miles'] > 0) {
                        
            // Do the conversion
            $journey['distance_km'] = $journey['distance_miles'] * 1.609;
            
            // Report that we did a conversion because it may introduce minor inaccuracy
            $journey['status'] = 101;
                        
            // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s');
        
        }
        
        // Convert days to hours
        if ($journey['hours_driven'] == null && $journey['days_driven'] != null) {
            
            // Do the conversion
            $journey['hours_driven'] = $journey['days_driven'] * 24;
            
            // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s');
            
        } else if ($journey['hours_driven'] !== null && $journey['days_driven'] == null) {
            
            // Instead, convert hours to days
            $journey['days_driven'] = round($journey['hours_driven'] / 24, 0); // we round to the nearest day
            
            // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s');
            
        }
        
        // Fix number of seats to 1 by default is there's no data
        
        if ($journey['seats'] == '0' || $journey['seats'] == null || $journey['seats'] == 0) {
            $journey['seats'] = 1;
            }

        
        // Look up if single trip
        
        // for eExpenses in particular, do a specific search for single and one way tickets 	
        
        if (isset($journey['provider']) && 	($journey['provider'] == 'eExpenses' || $journey['provider'] == 'old eExpenses') && $journey['is_return'] != null) {
		
            
            //search for specific strings identifying single journeys
            $searchstrings = array(
                'single',
                'one way',
            );
            
            foreach ($searchstrings as $values) {
                if (isset($journey['line_description']) && strpos(strtolower($journey['line_description']),$values) !== false) {
                    $journey['is_return'] = 'single';
                    break;
                    } elseif (isset($journey['flight_info']) && strpos(strtolower($journey['flight_info']),$values) !== false) {
                    $journey['is_return'] = 'single';
                    break;
                    } elseif (isset($journey['claim_notes']) && strpos(strtolower($journey['claim_notes']),$values) !== false) {
                    $journey['is_return'] = 'single';
                    break;
                    }
                // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s'); 
            }		
        }
    
    
        // Assign values to single or return journeys. Single journeys have a value of 1, return journeys have a value of 2.
		// we are looking for the words "single" and "return".
        
        // for Key travel, if we do not have return journeys specified then we need to assume that they are all singles
        
        if (strtolower($journey['provider']) == 'key travel') {
            if(strpos(strtolower($journey['is_return']),'return') !== false) {
            $journey['is_return'] = 2;
            } else {
            $journey['is_return'] = 1;
            }
        
        
        // for eExpenses, if we do not have return journeys specified then we need to assume that they are all returns (NOTE THIS IS THE OPPOSITE FROM KEY TRAVEL)
        } elseif ($journey['provider'] == 'eExpenses' || $journey['provider'] == 'old eExpenses' && (strtolower($journey['mode_of_transport']) == 'air' || strtolower($journey['mode_of_transport']) == 'rail') ) {
            if(strpos(strtolower($journey['is_return']),'single') !== false) {
            $journey['is_return'] = 1;
            } else {
            $journey['is_return'] = 2;	
            }
        
        // for all other providers, default journey is single
        } else {
            if(strpos(strtolower($journey['is_return']),'return') !== false) {
            $journey['is_return'] = 2;
            } else {
            $journey['is_return'] = 1;
            }
        }
        
        
        // Search for places in the origin
        
        if (strtolower($journey['origin']) == 'n/a' || strtolower($journey['origin']) == 'null' || strtolower($journey['origin']) == 'unknown') {
            $journey['origin'] = null; 
        }
        
        if (isset($journey['origin']) && $journey['origin'] != null && $journey['origin_latitude'] == null && $journey['origin_longitude'] == null) {
            
            if (strtolower($journey['origin']) == 'airport') {
                $journey['origin'] = 'EDI'; // We assume airport = Edinburgh airport?
            }
            
            // Try to get the journey
            $journey['origin_original'] = $journey['origin'];
            $origin_lookup = $this->PlaceLookup($journey['origin'], false, strtolower($journey['mode_of_transport']));
            if (isset($origin_lookup['name'])) {
                // We have a result
                // So we put it in the right place
                $journey['origin'] = $origin_lookup['name']; // because this is corrected if there are mispellings
                $journey['origin_latitude'] = $origin_lookup['latitude'];
                $journey['origin_longitude'] = $origin_lookup['longitude'];
                $journey['origin_country'] = $origin_lookup['country'];
                $journey['origin_how_calc'] = $origin_lookup['type'];
                
                // Update the last modified value
                $journey['last_updated'] = date('Y-m-d H:i:s');
            }
        }
    
        
        
        // Search for places in the destination
        
        if (strtolower($journey['destination']) == 'n/a' || strtolower($journey['destination']) == 'null' || strtolower($journey['destination']) == 'unknown') {
            $journey['destination'] = null; 
        }
        
        
        if (isset($journey['destination']) && $journey['destination'] != null && $journey['destination_latitude'] == null && $journey['destination_longitude'] == null) {
            
            if (strtolower($journey['destination']) == 'airport') {
                $journey['destination'] = 'EDI'; // We assume airport = Edinburgh airport?
                
            }
            
            
            // Try to get the journey
            $journey['destination_original'] = $journey['destination'];
            $destination_lookup = $this->PlaceLookup($journey['destination'],false,strtolower($journey['mode_of_transport']));
            if (isset($destination_lookup['name'])) {
                // We have a result
                // So we put it in the right place
                $journey['destination'] = $destination_lookup['name']; // because this is corrected if there are mispellings
                $journey['destination_latitude'] = $destination_lookup['latitude'];
                $journey['destination_longitude'] = $destination_lookup['longitude'];
                $journey['destination_country'] = $destination_lookup['country'];
                $journey['destination_how_calc'] = $destination_lookup['type'];
                
                // Update the last modified value
                $journey['last_updated'] = date('Y-m-d H:i:s');
            }
        }
        
    
        
        // Calculate distance from available information on latitude and longitude where we don't already have the distance
        // for example data from UoE eExpenses where no distance is supplied, only origin and destination
        
        if ($journey['distance_km'] == 0 && $journey['origin_latitude'] != null && $journey['origin_longitude'] != null && $journey['destination_latitude'] != null && $journey['destination_longitude'] != null) {
        
					
					// Do we already have a distance for the same lat lng?
					$distance_lookup =  (array) \DB::select('SELECT journey_id, distance_km, status FROM `journeys` WHERE origin_latitude = :origin_lat AND origin_longitude = :origin_lng AND destination_latitude = :destination_lat AND destination_longitude = :destination_lng AND status < :status AND is_return = 1 AND seats = 1 AND mode_of_transport = :mode GROUP BY distance_km
', ['mode' => $journey['mode_of_transport'], 'origin_lat' => $journey['origin_latitude'], 'origin_lng' => $journey['origin_longitude'], 'destination_lat' => $journey['destination_latitude'], 'destination_lng' => $journey['destination_longitude'], 'status' => $this->getStatusCodeToCheckBelow($journey['mode_of_transport'])]);
           
          if (isset($distance_lookup[0])) {$distance_lookup = (array) $distance_lookup[0];};
					
					if (isset($distance_lookup['distance_km']) && $distance_lookup['distance_km'] > 0 ) {
						$journey['distance_km'] = $distance_lookup['distance_km'];
					} else {
					
					
					// If not we do other methods
            
            if (strtolower($journey['mode_of_transport']) == 'air') {
                
                // Use the law of cosines method for single air journeys
                $journey['distance_km'] = round($this->SphericalLawOfCosines($journey['origin_latitude'],
                                                                        $journey['origin_longitude'],
                                                                        $journey['destination_latitude'],
                                                                        $journey['destination_longitude']),2);
                                                                        
                // Report that we used law of cosines method for distance calculation
                $journey['status'] = 110;											
                                                                                                                                                                                            
            } else if (strtolower($journey['mode_of_transport']) == 'rail') {
							
                // Use the Google API distance method for single journeys
                
                $journey['distance_km'] = round($this->GoogleDistance($journey['origin_latitude'],
                                        $journey['origin_longitude'],
                                        $journey['destination_latitude'],
                                        $journey['destination_longitude']),2);
                                        
                // Report that we used google distance for distance calculation
                $journey['status'] = 120;
            
            }
						
					}
            
        
        }

        // At this point we should have some kind of distance in distance_km column, we now copy that value to the
        // distance_km_processed column to do further processing on the distance without modifying the original value.
        // The original distance_km value is useful if we ever need to reprocess the same journey's distance
        $journey['distance_km_processed'] = $journey['distance_km'];

                
        
        //******** Rail ***********// 
        
        
        // if origin and destination are in the UK, set submode of transport to national rail
        
        if (strtolower($journey['mode_of_transport']) == 'rail' && $journey['submode_of_transport'] == null) {
        
            //Search for specific strings to identify country as UK
            $searchstrings = array(
                'gb',
                'europe/london',
                'uk',
                'united kingdom'
            );
            
            //Replace various spellings of UK with United Kingdom
            $journey['origin_country'] = trim(str_replace($searchstrings,'united kingdom',strtolower($journey['origin_country'])));
            $journey['destination_country'] = trim(str_replace($searchstrings,'united kingdom',strtolower($journey['destination_country'])));
            
    
                
                if(isset($journey['origin_country']) && strpos(strtolower($journey['origin_country']),'united kingdom') !== false && (isset($journey['destination_country']) && strpos(strtolower($journey['destination_country']),'united kingdom') !== false)){	
                    $journey['submode_of_transport'] = 'national rail';
                } else {
                    $journey['submode_of_transport'] = 'international rail';
                }
            
            // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s'); 	
            
        }		
            
        //******* Air ********//
        
        // Search for types of flight (domestic, short, long)
        
        if (strtolower($journey['mode_of_transport']) == 'air' && $journey['submode_of_transport'] == null && $journey['flight_type'] == null){ 
        
            //Search for specific strings to identify country as UK
            $searchstrings = array(
                'gb',
                'europe/london',
                'uk'
            );
            
            //Replace various spellings of UK with United Kingdom
            $journey['origin_country'] = str_replace($searchstrings,'united kingdom',strtolower($journey['origin_country']));
            $journey['destination_country'] = str_replace($searchstrings,'united kingdom',strtolower($journey['destination_country']));
            
            // If distance > 3700 km, it's a long haul
            if($journey['distance_km_processed'] > '3700') {
                $journey['flight_type'] = 'long';
            // Check the keywords to find domestic flights
            } elseif(isset($journey['origin_country']) && strpos(strtolower($journey['origin_country']),'united kingdom') !== false && (isset($journey['destination_country']) && strpos(strtolower($journey['destination_country']),'united kingdom') !== false)){	
                //if in UK, type of flight is Domestic
                $journey['flight_type'] = 'domestic';
                $journey['submode_of_transport'] = 'domestic average';	
            } else {
            // Everything else is Short
                $journey['flight_type'] = 'short';
                }
            // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s');	
        
        
        }
                    
                    
        // Search for classes of flight (economy, premium economy, business, first, average) in "flight_info", "line_description" and " claim_notes" fields
        
        //Search for specific strings to identify economy flights
            
        if (strtolower($journey['mode_of_transport']) == 'air' && $journey['submode_of_transport'] == null && $journey['flight_class'] == null){ 	
            
            $searchstrings = array(
                'ryanair',
                'econ',
                'easyjet',
                'standard',
                'flybe',
                'economy'
            );
        
        
            //Search for Premium Economy
            if (isset($journey['flight_info']) && strpos(strtolower($journey['flight_info']), 'premium economy') !== false) {
                $journey['flight_class'] = 'premium economy';
            } elseif (isset($journey['line_description']) && strpos(strtolower($journey['line_description']), 'premium economy') !== false) {
                $journey['flight_class'] = 'premium economy';
            } elseif (isset($journey['claim_notes']) && strpos(strtolower($journey['claim_notes']), 'premium economy') !== false) {
                $journey['flight_class'] = 'premium economy';	
            //Search for Business class	
            } elseif (isset($journey['flight_info']) && strpos(strtolower($journey['flight_info']),'business class') !== false) {
                $journey['flight_class'] = 'business';
            } elseif (isset($journey['line_description']) && strpos(strtolower($journey['line_description']),'business class') !== false) {
                $journey['flight_class'] = 'business';
            } elseif (isset($journey['claim_notes']) && strpos(strtolower($journey['claim_notes']),'business class') !== false) {
                $journey['flight_class'] = 'business';	
            //Search for First class		
            } elseif (isset($journey['flight_info']) && strpos(strtolower($journey['flight_info']),'first class') !== false) {
                $journey['flight_class'] = 'first';
            } elseif (isset($journey['line_description']) && strpos(strtolower($journey['line_description']),'first class') !== false) {
                $journey['flight_class'] = 'first';
            } elseif (isset($journey['claim_notes']) && strpos(strtolower($journey['claim_notes']),'first class') !== false) {
                $journey['flight_class'] = 'first';	
            } else {
                // Check the keywords to find Economy class
                foreach ($searchstrings as $values) {
                    if (isset($journey['flight_info']) && strpos(strtolower($journey['flight_info']),$values) !== false) {
                    $journey['flight_class'] = 'economy';
                    break;
                    } elseif (isset($journey['line_description']) && strpos(strtolower($journey['line_description']),$values) !== false) {
                    $journey['flight_class'] = 'economy';
                    break;
                    } elseif (isset($journey['claim_notes']) && strpos(strtolower($journey['claim_notes']),$values) !== false) {
                    $journey['flight_class'] = 'economy';
                    break;
                    } else {
                        // If can't find any keyword, choose Average
                        $journey['flight_class'] = 'average';
                    }
                }
                // Update the last modified value
                $journey['last_updated'] = date('Y-m-d H:i:s'); 
        
            }
            
            
        }
        
        //London leg//
        
        // 	Calculate London leg for single trips (London leg = 532.83 km) if either origin or destination = Edinburgh
        
        if ($journey['provider'] == 'eExpenses' || $journey['provider'] == 'old eExpenses') {
            if ($journey['flight_type'] == 'Long' && (strtolower($journey['origin']) == 'edinburgh' || strtolower($journey['origin']) == 'edi') && strtolower($journey['flight_type']) == 'single') {
                $journey['distance_km_processed'] = $journey['distance_km_processed'] + 532.83;
            } else if ($journey['flight_type'] == 'Long' && (strtolower($journey['destination']) == 'edinburgh' || strtolower($journey['destination']) == 'edi') && strtolower($journey['flight_type']) == 'single') {
                $journey['distance_km_processed'] = $journey['distance_km_processed'] + 532.83;
        
        // 	Calculate London leg for return trips (London leg = 1065.64 km) if either origin or destination = Edinburgh	
            } else if ($journey['flight_type'] == 'Long' && (strtolower($journey['origin']) == 'edinburgh' || strtolower($journey['origin']) == 'edi')) {
                $journey['distance_km_processed'] = $journey['distance_km_processed'] + 1065.64;
            } else if ($journey['flight_type'] == 'Long' && (strtolower($journey['destination']) == 'edinburgh' || strtolower($journey['destination']) == 'edi')) {
                $journey['distance_km_processed'] = $journey['distance_km_processed'] + 1065.64;
                
            // Update the last modified value
            $journey['last_updated'] = date('Y-m-d H:i:s');  
        }
        
    }	
        
                
        // Work out the sub mode of transport
    
        if (strtolower($journey['mode_of_transport']) == 'air' && ($journey['submode_of_transport'] == null)) {
        
            if (isset($journey['flight_type']) && isset($journey['flight_class'])) {
            
                $journey['flight_class'] = strtolower($journey['flight_class']);
            
                //remove 'haul' from the flight type
                $journey['flight_type'] = trim(str_replace('haul','',strtolower($journey['flight_type'])));	
                
                $journey['submode_of_transport'] = $journey['flight_type'].' '.$journey['flight_class'];
                
                // correct the exceptions 
                if (strtolower($journey['flight_type']) == 'domestic') {	
                    $journey['submode_of_transport'] = 'domestic average';
                } 		
                if (strtolower($journey['submode_of_transport']) == 'short premium economy') {
                    $journey['submode_of_transport'] = 'short economy';
                } 
                if (strtolower($journey['submode_of_transport']) == 'short first') {	
                    $journey['submode_of_transport'] = 'short business';
                } 	
                
                
                // Update the last modified value
                $journey['last_updated'] = date('Y-m-d H:i:s'); 
            }
        }
            
        
        // Get the total distance given by distance * number of seats*return
        
        if ($journey['distance_km_processed'] > 0) {
        $journey['distance_km_processed'] = $journey['distance_km_processed'] * $journey['is_return'] * $journey['seats'];
        } else {
        
        //distance smaller than 0 gives error
        $journey['status'] = 404;
        
        }
        
        
        
        // If we have a distance, we can do a defra_factors look up - this will return kg_co2e
        
        if ($journey['distance_km_processed'] > 0) {
            
            // Try a look up of the CO2e defra factors
            
            $defra_lookup = $this->DefraFactors((int) $journey['year'], $journey['mode_of_transport'], $journey['submode_of_transport'], (float) $journey['distance_km_processed'], $journey['provider']);
            
            
            if ($defra_lookup != false) {
                $journey['kg_co2e'] = $defra_lookup;
                            
                if ($journey['status'] == '0' || $journey['status'] == null || $journey['status'] == 0) {
                    
                    $journey['status'] = 100; // good calculation
                    
                }
                
                // Update the last modified time
                $journey['last_updated'] = date('Y-m-d H:i:s'); 
                
            } else {
                // Failure
            }
            
            $wtt_lookup = $this->DefraFactors($journey['year'],$journey['mode_of_transport'], $journey['submode_of_transport'], $journey['distance_km_processed'],$journey['provider'],true);
    
            
            if ($wtt_lookup != false) {
                $journey['wtt_kg_co2e'] = $wtt_lookup;
                                
                if ($journey['status'] == '0' || $journey['status'] == null || $journey['status'] == 0) {
                    
                    $journey['status'] = 100; // good calculation
                    
                }
                
                // Update the last modified time
                $journey['last_updated'] = date('Y-m-d H:i:s'); 
                
            } else {
                // Failure
            }
        }
        
        //Buses
        // convert to coaches all journeys whose cost is larger than 10£
        
    //    if (($journey['provider'] == 'eExpenses' || $journey['provider'] == 'old eExpenses') && strtolower($journey['mode_of_transport']) == 'bus' && $journey['cost'] > 10) {
    //    $journey['mode_of_transport'] = 'coach';
     //   }
    
        
    
        //***** Plugging the gaps ******//
        
        // if CO2 doesn't exist, or distance is missing and origin and destination are missing:
        if (($journey['kg_co2e'] == 0 && $journey['distance_km_processed'] == 0) || ($journey['origin'] != null && $journey['destination'] != null && $journey['origin'] == $journey['destination'])) {
            
            //for all modes except from Air spend factor =  km/£, for air = kg/£, hence for air calculations are slightly different
            //select all modes excluding air
            
            
            // Mode for searching
            $mode = strtolower($journey['mode_of_transport']) ?? null;
            $year = $journey['year'];
            global $spend_factors;
            if (strtolower($journey['mode_of_transport']) !== 'air' && $mode != null) {
                
            
                    //if we have a cost associated with the journey		
                    if(isset($journey['cost']) &&  $journey['cost'] !== null && $journey['cost'] !== 0 && $journey['cost'] !== '0') {
                        
                        // calculate distance from spend factor * spend
                        
                        
                       $journey['distance_km_processed'] = $spend_factors[$year][$mode][0]['spend_factor'] * $journey['cost'];
                        
                        
                        
                        
                        if ($journey['distance_km_processed'] > 0) {
                            // This worked
                            $journey['status'] = 300;
                            
                            // So now we do the defra factors?
                            $defra_lookup = $this->DefraFactors($journey['year'],$journey['mode_of_transport'], $journey['submode_of_transport'], $journey['distance_km_processed'], $journey['provider']);
                            $wtt_lookup = $this->DefraFactors($journey['year'],$journey['mode_of_transport'], $journey['submode_of_transport'], $journey['distance_km_processed'], $journey['provider'],true);
            
                            if ($defra_lookup != false && $wtt_lookup != false) {
                                $journey['kg_co2e'] = $defra_lookup;
                                $journey['wtt_kg_co2e'] = $wtt_lookup;	
                                
                            } else {
                                //Print error - unable to calculate distance due to missing mode of transport
                                $journey['status'] = 403;
                            }
                            
                        } else {
                            //Print error - unable to calculate distance due to missing spend information
                            $journey['status'] = 500;
                        }
                    }
                         
            } elseif (strtolower($journey['mode_of_transport']) == 'air') {
            
    
            $journey['kg_co2e'] = $spend_factors[$year][$mode]['kg_co2']['spend_factor']*$journey['cost'];
               $journey['wtt_kg_co2e'] = (float) $spend_factors[$year][$mode]['wtt_kg_co2']['spend_factor']* (float) $journey['cost'];
            $journey['status'] = 300;
    
            }	else {
               //Print error - unable to calculate distance due to missing mode of transport
               $journey['status'] = 403;
            }
        }
        
                    
        // Send back the edited data
        return ($journey);
    }
    
    
    /**
    * Try to return kg CO2e for a given distance, type and subtype
    *
    * @return array Google location data
    */
    
    //Get Defra factors for CO2 calculation
    
    private function DefraFactors($year, $mode, $submode, $distance, $provider = false, $wtt = false) {
        
        $return = false;
        
        if ($wtt == true) {
            // Use well-to-tank factor
            global $WTT_defra_factors;
            $defra_factors = $WTT_defra_factors;
        } else {
            global $defra_factors;
        }
        
        // Clean up for easier search
        $mode = strtolower($mode);
        $submode = strtolower($submode);
        $provider = strtolower($provider);
        if ($distance > 0) {
        
            if (isset($defra_factors[$year][$mode][$submode]) && $defra_factors[$year][$mode][$submode]['Defra_co2'] > 0) {
                // We have an easy match
                // Just return the result
                
                $return = ($defra_factors[$year][$mode][$submode]['Defra_co2']*$distance);
    
                
            } else if (isset($defra_factors[$year][$mode]) && count($defra_factors[$year][$mode]) == 1) {
                // We only have 1 single type of factor
                // So return that
                
                foreach($defra_factors[$year][$mode] as $i => $m) {
                    // return that calculation
                    $return = ($m['Defra_co2']*$distance);	
    
                    }
            } // etc.. need to add other cases
        } else {
            return false;
        }
        
        //Average occupancies
        
        if ($provider == 'hunters' || $provider == 'mckendry') {
                
            // if provider is Hunters, need to multiply defra factor by average passenger occupancy
            global $avg_occupancy;
    
            foreach($defra_factors[$year][$mode] as $i => $m) {
        
                // return that calculation
                $return = $return*$avg_occupancy['coach']['avg_occupancy'];
            }
                        
        }
        
        if ($provider == 'lothian buses' ) {
                
            // if provider is Hunters, need to multiply defra factor by average passenger occupancy
            global $avg_occupancy;
    
            foreach($defra_factors[$year][$mode] as $i => $m) {
        
                // return that calculation
                $return = $return*$avg_occupancy['bus']['avg_occupancy'];
            }	
                        
        }
    
        return $return;
    }
    
    
     /**
    * Actually do the work and process the CO2 emissions of a given journey
    *
    * @return array $journey (modified);
    */
    
    public function CurrentStatus() {
        $data = \DB::table('process_status')->latest('time')->limit(10)->get();
        return $data;
    }

    //
}
