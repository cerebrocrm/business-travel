<?php

/*
 *  This file is part of The University of Edinburgh
 *  Business Travel Reporting System.
 *
 *  Copyright 2017, 2018 University of Edinburgh
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class ViewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Start the database connection
    }
    
    /**
     * Display a view on screen
     *
     * @return string view contents
     */
    public function DisplayView($view = 'home', $title = 'Business Travel Report', $template = 'template', $year = null, $group_by = null, $group_by_var = null, $id = null) {
        // Set up the path for where the files are
        $path = str_replace('/app','',app()->path());
        
        if ($year != null && $view == 'home') {
            $view = 'year_report';
        }
        
        if ($group_by != null && $group_by_var != null) {
            $view = 'journey_report';
        }
        
        if ($id != null) {
            $view = 'single_journey';
        }
        
        if (isset($_GET['dept'])) {
            $dept = $_GET['dept'];
        } else {
            $dept = '';
        }
        
        
        if ($view == 'map' ||  $view == 'map-carbon') {
            $template = 'template_notitle';
        }
        
        // First get the template file which is the basis for our 
            $template_contents = file_get_contents($path.'/resources/views/'.$template.'.html');
            
            // Now get the view we want
            $view_contents = file_get_contents($path.'/resources/views/'.$view.'.html');
        
        if ($view == 'map' || $view == 'map-carbon') {
            
            $googleAPIKey = env('GOOGLE_MAPS_API_KEY', false);
            
            // Now get the view we want
           
            $view_contents = str_replace('%googleapikey%',$googleAPIKey,$view_contents);
            
        }
        
        
        

        // Replace the content in the template
        $template_contents = str_replace(array('%title%','%view%','%year%','%group_by%','%group_by_var%','%id%','%dept%'),array($title,$view_contents,$year,$group_by, urldecode($group_by_var), $id,$dept),$template_contents);
        
        // Then display it to the user
        return $template_contents;
    }
    
    
    public function DisplayViewEmbed ($view = 'home', $title = 'Business Travel Report', $template = 'template_notitle', $year = null, $group_by = null, $group_by_var = null, $id = null) {
        
        return $this->DisplayView($view, $title, 'template_notitle', $year, $group_by, $group_by_var , $id );
        
    }
    
    //
}
