<?php

/*
 *  This file is part of The University of Edinburgh
 *  Business Travel Reporting System.
 *
 *  Copyright 2017, 2018 University of Edinburgh
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class ReportController extends JourneyController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // department filtering
        
        global $dept_array;
        $dept_array == array();
        
        global $search_department;
        $search_department = '';
        
        if (isset($_GET['dept'])) {
            if ($_GET['dept'] && $_GET['dept'] != 'ALL_OF_THEM' && $_GET['dept'] != '') {
                // get CCL4 code
                $search_department = $_GET['dept'];
            
                // get the codes associated with this department
                $codes = \DB::table('cost_centres')->where('CCL4','like',$search_department)->orWhere('CCL5','like',$search_department)->orWhere('CCL6','like',$search_department)->orWhere('CCL3','like',$search_department)->orWhere('CCL2','like',$search_department)->groupBy('ccl6')->get();
                
                if (count ($codes) > 0) {
                    foreach ($codes as $row) {
                        $dept_array[] = $row->CCL6;
                    }
                }
                
    
            }
            
            
        }
        
        
        global $maxyear;
        $prefs = \DB::table('preferences')->where('pref_id','like','1')->get();
        $maxyear = $prefs[0]->maxyear;
        
        
    }
    
    
    /**
    * Calculates the great-circle distance between two points, with
    * the Law of Cosines formula.
    * Based on https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
    * @param float $latitudeFrom Latitude of start point in [deg decimal]
    * @param float $longitudeFrom Longitude of start point in [deg decimal]
    * @param float $latitudeTo Latitude of target point in [deg decimal]
    * @param float $longitudeTo Longitude of target point in [deg decimal]
    * @param float $earthRadius Mean earth radius in [m]
    * @return float Distance between points in [m] (same as earthRadius)
    * 
    */
    public function SphericalLawOfCosines ($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo); 	
        $lonTo = deg2rad($longitudeTo);
        
        $latDelta = $latFrom - $latTo;
        $lonDelta = $lonFrom - $lonTo;
        
        //$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        //cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        
        $angle	= acos(sin($latFrom) * sin($latTo) + cos($latTo) * cos($latFrom) * cos($lonFrom-$lonTo));
        return ($angle * $earthRadius)/1000;
   }
    
    
    /**
     * Journeys counted and summed by their mode of transport
     *
     * @return array results
     */
    public function JourneysByMode($year = null)
    {
        // Global arrays for department selection
        global $search_department;
        
        // Get data on general trends
        $data = array();
        
        // Different format depending on whether we're looking at one year or all of them
        if ($year == null) {
            // run this for all years
            $years = \DB::select('SELECT year FROM journeys WHERE 1 GROUP BY year ORDER BY year ASC');
            
            global $maxyear;
                        
            foreach ($years as $year) {
                
                if ( $year->year <= $maxyear ) {
                  $data[$year->year] = $this->JourneysByMode($year->year);
                }
                
            }

        } else {
            
            global $year_storage;
            $year_storage = $year;
            //\Cache::flush();
            
            $minutes = 24*60*2; // two days
            
            if (isset($_GET['nocache'])) {
                
                global $year_storage;
                global $dept_array;
                
                if (count($dept_array > 0) && $dept_array != null) {
                    $data = \DB::table('journeys')->select('mode_of_transport', 'submode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->whereIn('ccl6', $dept_array)->groupBy('mode_of_transport','submode_of_transport')->get();
                } else {
                    $data = \DB::table('journeys')->select('mode_of_transport', 'submode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->groupBy('mode_of_transport','submode_of_transport')->get();
                }
                
            } else {
               $data = \Cache::remember('journeys_by_mode_'.$year.$search_department, $minutes, function () {
                
                global $year_storage;
                global $dept_array;
                
                if (count($dept_array > 0) && $dept_array != null) {
                    return \DB::table('journeys')->select('mode_of_transport', 'submode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->whereIn('ccl6', $dept_array)->groupBy('mode_of_transport','submode_of_transport')->get();
                } else {
                    return \DB::table('journeys')->select('mode_of_transport', 'submode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->groupBy('mode_of_transport','submode_of_transport')->get();
                }
                
            });
            }
            
            
        }
        
        return $data;

    }
    
    /**
     * Journeys counted and summed by their mode of transport
     *
     * @return array results
     */
    public function JourneysByProvider($year = null)
    {
        // Get data on general trends
        $data = array();
        global $search_department;
        
        // Different format depending on whether we're looking at one year or all of them
        if ($year == null) {
            // run this for all years
            $years = \DB::select('SELECT year FROM journeys WHERE 1 GROUP BY year ORDER BY year ASC');
            
            foreach ($years as $year) {
                $data[$year->year] = $this->JourneysByProvider($year->year);
            }

        } else {
            
            global $year_storage;
            $year_storage = $year;
            //\Cache::flush();
            
            $minutes = 24*60*2; // two days
            
            if (isset($_GET['nocache'])) {
                  
               global $dept_array;
               
               if (count($dept_array > 0) && $dept_array != null) {
                   // ->whereIn('ccl6', $dept_array)
                   $data = \DB::table('journeys')->select('provider', 'mode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->whereIn('ccl6', $dept_array)->groupBy('provider','mode_of_transport')->get();
               } else {
                   $data = \DB::table('journeys')->select('provider', 'mode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->groupBy('provider','mode_of_transport')->get();
               }
               
                  
            
            } else {
                $data = \Cache::remember('journeys_by_provider2_'.$year.$search_department, $minutes, function () {
                
                global $year_storage;
                global $dept_array;
                
                if (count($dept_array > 0) && $dept_array != null) {
                    // ->whereIn('ccl6', $dept_array)
                    return \DB::table('journeys')->select('provider', 'mode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->whereIn('ccl6', $dept_array)->groupBy('provider','mode_of_transport')->get();
                } else {
                    return \DB::table('journeys')->select('provider', 'mode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))->where('year','=',$year_storage)->groupBy('provider','mode_of_transport')->get();
                }
                
                
            });
            }
           
            
        }
        
        return $data;

    }
    
    /**
     * Journeys counted and summed by their destination
     *
     * @return array results
     */
    public function JourneysByDestination($year = null)
    {
        // Get data on general trends
        $data = array();
        global $search_department;
        
        // Different format depending on whether we're looking at one year or all of them
        if ($year == null) {
            // run this for all years
            $years = \DB::select('SELECT year FROM journeys WHERE 1 GROUP BY year ORDER BY year ASC');
            
            foreach ($years as $year) {
                $data[$year->year] = $this->JourneysByMode($year->year);
            }

        } else {
            
            global $year_storage;
            $year_storage = $year;
            //\Cache::flush();
            
            $minutes = 24*60*2; // two days
            
            if (isset($_GET['nocache'])) {

                  global $dept_array;
                  
                  if (count($dept_array > 0) && $dept_array != null) {
                      // ->whereIn('ccl6', $dept_array)
                      $data = \DB::table('journeys')->select('destination',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                      ->where([
                          ['year','=',$year_storage],
                          ['destination', '<>', '0'],
                          ['destination', '<>', ''],
                      ])
                      ->whereIn('ccl6', $dept_array)
                      ->groupBy('destination')
                      ->orderBy('total_kg_co2e','desc')
                      ->limit(100)->get();
                  } else {
                      $data = \DB::table('journeys')->select('destination',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                      ->where([
                          ['year','=',$year_storage],
                          ['destination', '<>', '0'],
                          ['destination', '<>', ''],
                      ])
                      ->groupBy('destination')
                      ->orderBy('total_kg_co2e','desc')
                      ->limit(100)->get();
                  }
            } else {
               $data = \Cache::remember('journeys_by_destination_'.$year.$search_department, $minutes, function () {
                
                  global $year_storage;
                  global $dept_array;
                  
                  
                  if (count($dept_array > 0) && $dept_array != null) {
                      // ->whereIn('ccl6', $dept_array)
                      return \DB::table('journeys')->select('destination',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                      ->where([
                          ['year','=',$year_storage],
                          ['destination', '<>', '0'],
                          ['destination', '<>', ''],
                      ])
                      ->whereIn('ccl6', $dept_array)
                      ->groupBy('destination')
                      ->orderBy('total_kg_co2e','desc')
                      ->limit(100)->get();
                  } else {
                      return \DB::table('journeys')->select('destination',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                      ->where([
                          ['year','=',$year_storage],
                          ['destination', '<>', '0'],
                          ['destination', '<>', ''],
                      ])
                      ->groupBy('destination')
                      ->orderBy('total_kg_co2e','desc')
                      ->limit(100)->get();
                  }
                  
                  
                  
              });
            }
            
            
            
            
        }
        
        return $data;

    }
    
    /**
     * Journeys counted and summed by an arbitrary grouping
     *
     * @return array results
     */
    public function GroupedJourneyReport($group_by,$year = null)
    {
        // Get data on general trends
        $data = array();
        global $search_department;
        
        // Different format depending on whether we're looking at one year or all of them
        if ($year == null) {
            // run this for all years
            $years = \DB::select('SELECT year FROM journeys WHERE 1 GROUP BY year ORDER BY year ASC');
            
            foreach ($years as $year) {
                $data[$year->year] = $this->GroupedJourneyReport($group_by,$year->year);
            }

        } else {
            
            global $year_storage;
            $year_storage = $year;
            
            global $group_by_storage;
            $group_by_storage = $group_by;
            //\Cache::flush();
            
            $minutes = 24*60*2; // two days
            
            if (isset($_GET['nocache'])) {
               
                              
                global $year_storage;
                global $group_by_storage;
                global $dept_array;
                
                if ($group_by_storage == 'status') {
                  $unfinished = \DB::table('journeys')->select($group_by_storage,\DB::raw('count(*) as total'))
                    ->where([
                        ['year','=',$year_storage],
                        ['status', '=', ''],
                     ])
                    ->orWhere('status', 0)
                    ->get();
                  
                  $data = \DB::table('journeys')->select($group_by_storage,\DB::raw('count(*) as total'))
                    ->where([
                        ['year','=',$year_storage],
                        [$group_by_storage, '<>', '0'],
                        [$group_by_storage, '<>', ''],

                    ])
                    ->groupBy($group_by_storage)
                    ->get();
                    
                  $data[count($data)] = $unfinished[0];
                  
                } else if (count($dept_array > 0) && $dept_array != null) {
                    // ->whereIn('ccl6', $dept_array)
                    $data =  \DB::table('journeys')->select($group_by_storage,\DB::raw('count(*) as total'))
                    ->where([
                        ['year','=',$year_storage],
                        [$group_by_storage, '<>', '0'],
                        [$group_by_storage, '<>', ''],

                    ])
                    ->whereIn('ccl6', $dept_array)
                    ->groupBy($group_by_storage)->get();
                } else {
                    $data = \DB::table('journeys')->select($group_by_storage,\DB::raw('count(*) as total'))
                    ->where([
                        ['year','=',$year_storage],
                        [$group_by_storage, '<>', '0'],
                        [$group_by_storage, '<>', ''],

                    ])
                    ->groupBy($group_by_storage)->get();
                }
            
            } else {
                $data = \Cache::remember('journeys_by_'.$group_by.'_'.$year.$search_department, $minutes, function () {
                
                global $year_storage;
                global $group_by_storage;
                global $dept_array;
                
                if (count($dept_array > 0) && $dept_array != null) {
                    // ->whereIn('ccl6', $dept_array)
                    return \DB::table('journeys')->select($group_by_storage,\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                    ->where([
                        ['year','=',$year_storage],
                        [$group_by_storage, '<>', '0'],
                        [$group_by_storage, '<>', ''],
                    ])
                    ->whereIn('ccl6', $dept_array)
                    ->groupBy($group_by_storage)->get();
                } else {
                    return \DB::table('journeys')->select($group_by_storage,\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                    ->where([
                        ['year','=',$year_storage],
                        [$group_by_storage, '<>', '0'],
                        [$group_by_storage, '<>', ''],
                    ])
                    ->groupBy($group_by_storage)->get();
                }
            
                
            });
            }
            
           
        }
        
        return $data;
        
    }
    
    
    /**
     * Journeys counted and summed by their destination
     *
     * @return array results
     */
    public function JourneysByDestinationMinusLondonLeg($year = null)
    {
        // Get data on general trends
        $data = array();
        global $search_department;
        
        //\Cache::flush();
        
        // Different format depending on whether we're looking at one year or all of them
        if ($year == null) {
            // run this for all years
            $years = \DB::select('SELECT year FROM journeys WHERE 1 GROUP BY year ORDER BY year ASC');
            
            foreach ($years as $year) {
                $data[$year->year] = $this->JourneysByModeMinusLondonLeg($year->year);
            }

        } else {
            
            global $year_storage;
            $year_storage = $year;
            //\Cache::flush();
            
            $minutes = 24*60*2; // two days
            
            $data = \Cache::remember('journeys_by_destination_final_'.$year.$search_department, $minutes, function () {
                
                global $year_storage;
                global $dept_array;
                
                $check_data = null;
                $return_data = null;
                $ids_to_check = array();
                $journey_log = array();
                
                
                if (count($dept_array > 0) && $dept_array != null) {
                    // ->whereIn('ccl6', $dept_array)
                    $check_data =  \DB::table('journeys')->select('supplier_job_id','ccl6', \DB::raw('count(journey_id) as counta'), \DB::raw('count(DISTINCT supplier_job_id) as people'),  \DB::raw('count(journey_id)/count(DISTINCT supplier_job_id) as ratio'))
                    ->where([
                        ['year','=',$year_storage],
                        ['destination', '<>', '0'],
                        ['destination', '<>', ''],
                        ['mode_of_transport', 'Like', 'air'],
                        ['provider', 'Like', 'Key Travel'],
                    ])
                    ->whereIn('ccl6', $dept_array)
                    ->groupBy('supplier_job_id')
                    ->get();
                } else {
                    $check_data = \DB::table('journeys')->select('supplier_job_id','ccl6', \DB::raw('count(journey_id) as counta'), \DB::raw('count(DISTINCT supplier_job_id) as people'),  \DB::raw('count(journey_id)/count(DISTINCT supplier_job_id) as ratio'))
                    ->where([
                        ['year','=',$year_storage],
                        ['destination', '<>', '0'],
                        ['mode_of_transport', 'Like', 'air'],
                        ['provider', 'Like', 'Key Travel'],
                    ])
                    ->groupBy('supplier_job_id')
                    ->get();
                }
                
                
                foreach ($check_data as $row) {
                    $ids_to_check[] = $row->supplier_job_id;
                }
                
                if (count($dept_array > 0) && $dept_array != null) {
                    // ->whereIn('ccl6', $dept_array)
                    $process = \DB::table('journeys')->select('journey_id','ccl6', 'supplier_job_id','destination_latitude','destination_longitude')
                    ->where([
                        ['year','=',$year_storage],
                        ['destination', '<>', '0'],
                        ['destination', '<>', ''],
                    ])
                    ->whereIn('supplier_job_id', $ids_to_check)
                    ->whereIn('ccl6', $dept_array)
                    ->get();
                } else {
                    $process = \DB::table('journeys')->select('journey_id','supplier_job_id','destination_latitude','destination_longitude')
                    ->where([
                        ['year','=',$year_storage],
                        ['destination', '<>', '0'],
                        ['destination', '<>', ''],
                    ])
                    ->whereIn('supplier_job_id', $ids_to_check)
                    ->get();
                }
                
                
                foreach ($process as $journey) {
                    
                    if ($journey->destination_latitude != null && $journey->destination_longitude != null) {
                        // Edinburgh: 55.9533° N, 3.1883° W - we check how far away this is
                        $distance = \App\Http\Controllers\JourneyProcessor::SphericalLawOfCosines(55.9533, 3.1883, $journey->destination_latitude, $journey->destination_longitude);
                        
                        
                        if (isset($journey_log[$journey->supplier_job_id])) {
                            
                            if ($journey_log[$journey->supplier_job_id]['distance'] < $distance) {
                                $journey_log[$journey->supplier_job_id]['distance'] = $distance;
                                $journey_log[$journey->supplier_job_id]['journey_id'] = $journey->journey_id;
                            }
                        } else {
                            $journey_log[$journey->supplier_job_id] = array(
                                'distance' => $distance,
                                'journey_id' => $journey->journey_id
                            );
                        }
                    }
                    
                }
                
                $journeys_to_return = array_column($journey_log, 'journey_id');
                
                
                if (count($dept_array > 0) && $dept_array != null) {
                    // ->whereIn('ccl6', $dept_array)
                    return \DB::table('journeys')->select('destination','destination_latitude','destination_longitude',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'))
                    ->where([
                        ['year','=',$year_storage],
                        ['destination', '<>', '0'],
                        ['destination', '<>', ''],
                        ['destination', 'Not Like', 'Edinburgh'],
                        ['provider', 'Not Like', 'Key Travel'],
                        ['mode_of_transport', 'Like', 'air'],
                    ])
                    ->whereIn('ccl6', $dept_array)
                    ->whereIn('journey_id', $journeys_to_return, 'or')
                    ->having('destination','Not Like', 'Edinburgh')
                    ->groupBy('destination_latitude','destination_longitude')
                    ->orderBy('total','desc')
                    ->limit(5000)->get();
                    
                } else {
                    return \DB::table('journeys')->select('destination','destination_latitude','destination_longitude',\DB::raw('count(*) as total'),  \DB::raw('sum(kg_co2e) as total_kg_co2e'))
                    ->where([
                        ['year','=',$year_storage],
                        ['destination', '<>', '0'],
                        ['destination', '<>', ''],
                        ['destination', 'Not Like', 'Edinburgh'],
                        ['provider', 'Not Like', 'Key Travel'],
                        ['mode_of_transport', 'Like', 'air'],
                    ])
                    ->whereIn('journey_id', $journeys_to_return, 'or')
                    ->having('destination','Not Like', 'Edinburgh')
                    ->groupBy('destination_latitude','destination_longitude')
                    ->orderBy('total','desc')
                    ->limit(5000)->get();
                }
                
                
                
                
            });
            
            
        }
        
        return $data;

    }
    
    
    /**
     * Return a list of departments for filtering
     *
     * @return array results
     */
    public function LondonReport($year = null)
    {
        // Get data on general trends
        $data = array();
        global $search_department;
        
        //\Cache::flush();
        
        // Different format depending on whether we're looking at one year or all of them
        if ($year == null) {
            // run this for all years
            $years = \DB::select('SELECT year FROM journeys WHERE 1 GROUP BY year ORDER BY year ASC');
            
            foreach ($years as $year) {
                $data[$year->year] = $this->LondonReport($year->year);
            }

        } else {
            
            global $year_storage;
            $year_storage = $year;
            //\Cache::flush();
            
            $minutes = 24*60*2; // two days
            
            $data = \Cache::remember('london_report2'.$year.$search_department.rand(0,10000000), $minutes, function () {
                
                global $year_storage;
                global $dept_array;
                
                $check_data = null;
                $return_data = null;
                $ids_to_check = array();
                
                
                $check_data = \DB::table('journeys')->select('supplier_job_id','ccl6', \DB::raw('count(journey_id) as counta'), \DB::raw('count(DISTINCT supplier_job_id) as people'),  \DB::raw('count(journey_id)/count(DISTINCT supplier_job_id) as ratio'))
                ->where([
                    ['year','=',$year_storage],
                    ['destination', '<>', '0'],
                    ['mode_of_transport', 'Like', 'air'],
                    ['provider', 'Like', 'Key Travel'],
                ])
                ->having('ratio','>', 2)
                ->groupBy('supplier_job_id')
                ->get();
                
                
                
                foreach ($check_data as $row) {
                    $ids_to_check[] = $row->supplier_job_id;
                }
                
                
                if (count($dept_array > 0) && $dept_array != null) {
                    return \DB::table('journeys')
                    ->select('mode_of_transport', 'ccl6', 'supplier_job_id', \DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                    ->whereRaw('year = ? AND (origin Like "Edinburgh" OR origin Like "Waverly" OR origin Like "%Waverly%" OR origin Like "%Edinburgh%"  OR origin Like "Haymarket" OR origin Like "%Haymarket%" OR origin Like "%Glasgow%" OR origin Like "Glasgow") AND ( destination Like "%London%" OR destination Like "London Kings Cross"  OR destination Like "%Kings Cross%" OR destination Like "%Gatwick%" OR destination Like "Heathrow" OR destination Like "Luton" OR destination Like "Stanstead" OR destination Like "%Heathrow%" OR destination Like "%Luton%" OR destination Like "%Stanstead%" )', [$year_storage])
                    ->whereNotIn('supplier_job_id',$ids_to_check)
                    ->whereIn('ccl6', $dept_array)
                    ->groupBy('mode_of_transport')
                    ->get();
                } else {
                    return \DB::table('journeys')
                    ->select('mode_of_transport',\DB::raw('count(*) as total'), \DB::raw('sum(kg_co2e) as total_kg_co2e'), \DB::raw('sum(wtt_kg_co2e) as total_wtt_kg_co2e'), \DB::raw('sum(cost) as total_cost'), \DB::raw('sum(distance_km_processed) as total_distance_km_processed'))
                    ->whereRaw('year = ? AND (origin Like "Edinburgh" OR origin Like "Waverly" OR origin Like "%Waverly%" OR origin Like "%Edinburgh%"  OR origin Like "Haymarket" OR origin Like "%Haymarket%" OR origin Like "%Glasgow%" OR origin Like "Glasgow") AND ( destination Like "%London%" OR destination Like "London Kings Cross"  OR destination Like "%Kings Cross%" OR destination Like "%Gatwick%" OR destination Like "Heathrow" OR destination Like "Luton" OR destination Like "Stanstead" OR destination Like "%Heathrow%" OR destination Like "%Luton%" OR destination Like "%Stanstead%" )', [$year_storage])
                    ->whereNotIn('supplier_job_id',$ids_to_check)
                    ->groupBy('mode_of_transport')
                    ->get();
                }  
                
                
            });
            
            
        }
        
        return $data;
        
    }
    
    /**
     * Return a list of departments for filtering
     *
     * @return array results
     */
    public function Departments()
    {
        global $search_department;
        
        $data = \DB::table('cost_centres')->select('ccl4', 'ccn4')->where('ccn4', '<>', '')->groupBy('ccn4')->orderBy('ccn4', 'asc')->get();
        
        foreach ($data as $id => $row) {
            if ($search_department == $row->ccl4) {
                $data[$id]->selected = 'selected';
            }
        }
        
        return $data;
        
    }
    
    
    //
}
