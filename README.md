# Business Travel Reporting System

This is a system to automatically analyse business travel data provided in a standard format.

## System requirements

* PHP 7.1
* MySQL Server, or equivalent compatible SQL Server (such as MariaDB)
* Cron, or equivalent task scheduler

## Framework

The system is built in Laravel Lumen framework and relies upon a number of different sources to automatically determine the origin and destination and correct CO2 emissions of different journeys.  For instance the system will connect to the Google Maps API to get distances between two points using road, rail or flight.

## Journey Processing

The "brains of the operation" (so to speak) is the Journey Processor class.  All data is imported as journeys, each of these "journey records" is processed by the Journey Processor which applies a number of translations to the data in order to determine the final distance and emissions of that journey.

Journeys are processed on a Cron Job (see the section about Cron Jobs).

## Cron Jobs

Multiple Cron Jobs are used to perform various actions and processing on the data.

* */10 * * * * php public/cost-centres.php
* */3 * * * * wget /process/journeys

## Copyright notice

Copyright (C) The University of Edinburgh 2017, 2018

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
